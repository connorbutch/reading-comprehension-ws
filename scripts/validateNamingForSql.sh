#TODO in future Get the absolute path of script, so regardless of where script is invoked from, we can use it (otherwise, things get tricky)
#ABSOLUTE_PATH=${BASH_SOURCE[0]}
#cd $ABSOLUTE_PATH

#Define our exit codes for visibility
EXIT_SUCCESS=0 #just to make explicit
EXIT_FAILED_MAVEN_VERSION=1
EXIT_FILENAME_VIOLATES_CONVENTION=2

#Used to print message if debug is enabled
function debug() {
	if [[ $DEBUG ]]; then
		echo $1
	fi
}

#Get debug, default to true, but let be overwritten by defining environment variable "DEBUG"
DEBUG=0
if [[ -z "${DEBUG}" ]]; then
	DEBUG=1
fi

#Get the maven version using the help plugin
MAVEN_VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)

#Check status, if not succesful, then exit
if [ $? ]; then
	debug "Successfully got maven version $MAVEN_VERSION"
else
	echo "Failed to get maven version"
	exit EXIT_FAILED_MAVEN_VERSION
fi

#Trim snapshot from maven version, if snapshot is not present, will have no effect
MAVEN_VERSION=${MAVEN_VERSION//"-SNAPSHOT"/}
debug "Maven version (without snapshot) is $MAVEN_VERSION"

#Build the regex for an allowable filename, which must be named based on maven/docker version
FILE_REGEX="^((${MAVEN_VERSION})(-)[0-9]+)(.sql)[ ]*)$"
debug "Regex for sql files is $FILE_REGEX"

#Get to the correct directory -- makes looping much easier
cd src/db

#Loop over files and ensure they all match the proper format
for filename in *
do
	debug "Examining file $filename"
	if [[ "$filename" =~ $FILE_REGEX ]]; then
		debug "Filename $filename matches regex and is valid"
		#TODO in future, this is where we call restful webservice for sql validation
	elif [[ "$filename" = "archive.sql" ]] || [[ "$filename" = "testData.sql" ]] || [[ "$filename" = "sqlForIntegrationTest.sql" ]]; then
		debug "Filename matches one to skip regex validation"
	else
		echo "Filename $filename is NOT valid"
		exit 1 # EXIT_FILENAME_VIOLATES_CONVENTION
	fi
done

debug "All files match naming convention"
exit 0 #EXIT_SUCCESS