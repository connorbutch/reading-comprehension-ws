#NOTE: intentionally stayed away from "shebang" here for portability

#This file gets the service url of the knative service for use when load testing
kubectl get ksvc reading-comprehension-ws

#Output will be something like this, so we need to parse it to only extract the URL
#NAME            URL                                                LATESTCREATED         LATESTREADY           READY   REASON
#helloworld-go   http://helloworld-go.default.34.83.80.117.xip.io   he