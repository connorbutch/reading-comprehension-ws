#NOTE: intentionally stayed away from "shebang" here for portability

#TODO in future Get the absolute path of script, so regardless of where script is invoked from, we can use it (otherwise, things get tricky)
#ABSOLUTE_PATH=${BASH_SOURCE[0]}
#cd $ABSOLUTE_PATH

#The destination file where all combined sql should live
FILE_NAME=sqlForIntegrationTest.sql
SEPARATOR=----------------------------------------------------------------------------------------------------------

#Delete file and recreate file (if exists) so we get a clean start
cd src/db/
rm --force $FILE_NAME
touch $FILE_NAME

#Loop over files in directory, copying archive first, then sql files in order, then test data file
for filename in *
do
	if [[ $filename != $FILE_NAME ]]; then
		#NOTE: for whatever reason, mysql test containers may behave weirdly with comments; in future, if you encounter issues, comment the below lines in this file (which prevent comments from being in sql)
		echo $SEPARATOR >> $FILE_NAME
		echo --$filename >> $FILE_NAME
		cat $filename >> $FILE_NAME
		echo >> $FILE_NAME
	fi
done

#make explicit
exit 0 