#Use openjdk 11 as our base image (java support)
FROM openjdk:11-jdk
#In the future, if we want to access logs, we should use this mount
VOLUME /tmp
#TODO how do we adjust for changing versions . . . 
ARG JAR_FILE=/target/reading-comprehension-ws-0.0.1-SNAPSHOT.jar
#Copy our jar file into the file "app.jar"
COPY ${JAR_FILE} app.jar
#Assign from environment variables to docker environment variables, so can be accessed in container
#ENV DB_JDBC_URL $DB_JDBC_URL
#ENV DB_USERNAME $DB_USERNAME
#ENV DB_PASSWORD $DB_PASSWORD
#run the jar file, which starts our spring boot app
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"] 

#this doesn't do anything, but it is a reminder we should expose port 8080 with docker run -p 8080 . . . for this application
EXPOSE 8080
#setup (internal) container health check
HEALTHCHECK CMD curl --fail localhost:8080/health || exit 1