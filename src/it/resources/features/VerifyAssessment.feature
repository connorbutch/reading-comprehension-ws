#Author: Connor Butch
Feature: Verify an assessment


 #NOTE: this looks very similar, but I prefer to break up for many reasons
  Scenario Outline: A request to verify a non-existent assessment should yield an error.
   Given a request is made to verify an assessment 
   And the isbn of the assessment to verify is <isbn>
   And the verified value is "<isVerified>"   
   When the request is made 
   Then an error response will be returned
   And the status code will be 404

    Examples: 
    | isbn  |isVerified|
    | 14    |true      |
    | 2312  |false     |
    
    Scenario Outline: A request is made to verify an existing assessment should update the assessment's verified value.
   Given a request is made to verify an assessment 
   And the isbn of the assessment to verify is <isbn>
   And the verified value is "<isVerified>"
	 When the request is made 
   Then an assessment will be returned
	 And the isbn will match the isbn sent above
	 And the author's first name will be "<authorFirstName>"
	 And the author's last name will be "<authorLastName>"
	 And the title will be "<title>"
	 And the number of points will be <numberOfPoints>
	 And the reading level will be <readingLevel>	   
	 And the created teacher id will be <createdTeacherId>
	 And the verified value will be the same as the one sent above
	   
	   Examples: 
    | isbn              |isVerified|authorFirstName|authorLastName|title         |numberOfPoints|readingLevel|createdTeacherId|
    | 9781976530739     | true     |Herman        |    Melleville|Moby Dick     |   65         |    10.8    |  1             |
    | 9781976530739     | true     |Herman        |    Melleville|Moby Dick     |   65         |    10.8    |  1             |
    | 9781976530739     | false    |Herman        |    Melleville|Moby Dick     |   65         |    10.8    |  1             |
    | 9781976530739     | false    |Herman        |    Melleville|Moby Dick     |   65         |    10.8    |  1             |