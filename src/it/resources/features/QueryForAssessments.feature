#Author: Connor Butch
Feature: Retrieving multiple assessments.


  Scenario Outline: An invalid request to retrieve assessments should yield an error.
    Given a request is made to retrieve assessments
    And the limit passed is <limit>
    And the offset passed is <offset>
    And the created teacher id is <createdTeacherId>
    When the request is made
    Then an error response will be returned
    And the status code will be 400
    

    Examples: 
      | limit  | offset | createdTeacherId  |
      | -1     |     5  | 1                 |
      | 2      |     -4 | 1                 |
      |1       | 2      | -1                |

   Scenario Outline: A valid request with criteria that doesn't match any assessments should return an empty list.
   Given a request is made to retrieve assessments
    And the limit passed is <limit>
    And the offset passed is <offset>
    And the created teacher id is <createdTeacherId>
    When the request is made
    Then an empty list will be returned
    And the status code will be 200
    And the total number of assessment header will be 0
    
    Examples: 
      | limit  | offset | createdTeacherId  |
      | 2      |     0  | 12                |
      | 10     |     99 | 17                |
      
      
      Scenario Outline: A valid request should return assessments that match the criteria.
      Given a request is made to retrieve assessments
      And the limit passed is <limit>
      And the offset passed is <offset>
      And the created teacher id is <createdTeacherId>
      When the request is made
      Then a non-empty list will be returned
      And one of the assessments will be for the book with title "<title>"
      #The below two fields can, and often will be different.  Total number header always returns the total number, but limit and offset let you limit what to return
      And the number of assessments returned will be <numberReturned>
      And the total number of assessment header will be <totalNumberAssessments>
      
      Examples: 
      | limit  | offset | createdTeacherId  |title|numberReturned|totalNumberAssessments|
      |2       |0       |14                 |a    |2             | 3                    |
      #TODO add more here
      