#Author: Connor Butch
#TODO add functionality to distinguish between null and empty strings
Feature: Add an assessment for a book.

  Scenario Outline: An invalid request to add an assessment should yield an error.
    Given a request is made to add an assessment
    And the isbn to be added (path parameter) is <isbn>
    And the isbn in the body is <isbnInBody> 
    #NOTE: anything but true, will be considered false
    And the test verified value is "<verified>"
    And the author first name is "<authorFirstName>"
    And the author last name is "<authorLastName>"
    And the title is "<title>"
    And the reading level is <readingLevel>
    And the number of  points is <numberOfPoints>
    And the created teacher id is <teacherId>
    When the request is made 
    Then an error response will be returned
    And the status code will be 400
    #NOTE: I always debate whether or not to verify error messages, or if I should use a custom code (in addition to http status code) to differentiate errors
    
    #ISBN path parameter cannot be null (NOTE: INTENTIONALLY SKIPPED -- THIS DOESN'T ROUTE TO OUR CODE, SO ALLOW TO COME BACK AS 500)
    #Assessment body cannot be null      SKIPPED FOR NOW
    #Assessment isbn must match path parameter isbn
    #Assessment cannot be sent in as verified
    #Author first name cannot be null
    #Author last name cannot be null
    #Title cannot be null
    #Reading level cannot be null
    #Reading level must be above min
    #Reading level must be below max
    #Points cannot be null
    #Points must be above min
    #Points must be below max
    #Created teacher id cannot be null
    
    
    Examples:
    |isbn|isbnInBody|verified|authorFirstName|authorLastName|title|readingLevel|numberOfPoints|teacherId|    
    | 2  |3         |  false | Connor        | Butch        | asd |   9.8      |   28         |    14   |
    | 2  |3         |  true  | Connor        | Butch        | asd |   9.8      |   28         |    14   |
    | 2  |3         |  false |               | Butch        | asd |   9.8      |   28         |    14   |
    | 2  |3         |  false | Connor        |              | asd |   9.8      |   28         |    14   |
    | 2  |3         |  false | Connor        | Butch        |     |   9.8      |   28         |    14   |
    | 2  |3         |  false | Connor        | Butch        | asd |            |   28         |    14   |
    | 2  |3         |  false | Connor        | Butch        | asd |   -2       |   28         |    14   |
    | 2  |3         |  false | Connor        | Butch        | asd |   999      |   28         |    14   |
    | 2  |3         |  false | Connor        | Butch        | asd |   9.8      |              |    14   |
    | 2  |3         |  false | Connor        | Butch        | asd |   9.8      |   -28        |    14   |
    | 2  |3         |  false | Connor        | Butch        | asd |   9.8      |   280        |    14   |
    | 2  |3         |  false | Connor        | Butch        | asd |   9.8      |   28         |         |
       
    
    

Scenario Outline: A request is made to add an assessment for a book that already has one should yield an error.
    Given a request is made to add an assessment
	  And the isbn to be added (path parameter) is <isbn>
	   And the isbn in the body is <isbnInBody> 
    #NOTE: anything but true, will be considered false
    And the test verified value is "<verified>"
    And the author first name is "<authorFirstName>"
    And the author last name is "<authorLastName>"
    And the title is "<title>"
    And the reading level is <readingLevel>
    And the number of  points is <numberOfPoints>
    And the created teacher id is <teacherId>
    When the request is made 
    Then an error response will be returned
    And the status code will be 409
	  
	  Examples:
	  |isbn          |isbnInBody     |verified|authorFirstName|authorLastName|title|readingLevel|numberOfPoints|teacherId|
    |9781976530739 |9781976530739  |  false |       c       |       asd    | tie |   2.2      |     .6       |   7221  |
	  
	  
Scenario Outline: A valid request is made to add an assessment should succeed.
    Given a request is made to add an assessment
	  And the isbn to be added (path parameter) is <isbn>
	  And the isbn in the body is <isbnInBody> 
    #NOTE: anything but true, will be considered false
    And the test verified value is "<verified>"
    And the author first name is "<authorFirstName>"
    And the author last name is "<authorLastName>"
    And the title is "<title>"
    And the reading level is <readingLevel>
    And the number of  points is <numberOfPoints>
    And the created teacher id is <teacherId>
	  When the request is made 
	  Then a successful response will be returned
	  #NOTE: I prefer to explicitly spell out every attribute so that we are ensured we don't miss anything	  
	  And the isbn returned will be the same as one passed
	  And the verified value will be false
	  And the author first name will be the same as one passed
	  And the author last name will be the same as one passed
	  And the title will be the same as one passed
	  And the reading level will be the same as one passed
	  And the number of points will be the same as one passed
	  And the created teacher id will be the same as one passed
	  
	    Examples:
	  |isbn|isbnInBody|verified|authorFirstName|authorLastName|title|readingLevel|numberOfPoints|teacherId|
	  |1234|1234      |false   | connor        | butch        |test | 7.5        | 5            | 7       |