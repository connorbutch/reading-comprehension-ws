#Author: Connor Butch
Feature: Get assessment by isbn.

	 #NOTE: this looks very similar, but I prefer to break up for many reasons
  Scenario Outline: An invalid request to retrieve an assessment by isbn should yield an error.
    Given a request is made to retrieve assessment by isbn
    And the isbn searched on is <isbn>
    When the request is made 
    Then an error response will be returned
    And the status code will be 400
    
    Examples:
    |isbn|
    |-232|
    |-2  |

 #NOTE: this looks very similar, but I prefer to break up for many reasons
  Scenario Outline: A request to retrieve an assessment for a book that does not yet have an assessment will yield an error.
   Given a request is made to retrieve assessment by isbn
   And the isbn searched on is <isbn>
   When the request is made 
   Then an error response will be returned
   And the status code will be 404

    Examples: 
    | isbn  |
    | 14    |
    | 2312  |
    
    Scenario Outline: A request is made to retrieve an existing assessment
     Given a request is made to retrieve assessment by isbn
     And the isbn searched on is <isbn>
	   When the request is made 
	   Then an assessment will be returned
	   And the isbn will match the isbn sent above
	   And the author's first name will be "<authorFirstName>"
	   And the author's last name will be "<authorLastName>"
	   And the title will be "<title>"
	   And the number of points will be <numberOfPoints>
	   And the reading level will be <readingLevel>
	   #NOTE: anything but exactly "true" is considered false
	   And the verification value will be "<isVerified>"
	   And the created teacher id will be <createdTeacherId>
	   And the href link will end with "<href>"
	   Examples: 
    | isbn              |authorFirstName|authorLastName|title         |numberOfPoints|readingLevel|isVerified |createdTeacherId|href                                   |
    | 9781976530739     | Herman        |    Melleville|Moby Dick     |   65         |    10.8    |false      |  1             |assessments/9781976530739/9781976530739|
    
    
    