#Author: Connor Butch
#TODO add functionality to distinguish between null and empty strings
Feature: Add a question for an assessment

	

  Scenario Outline: An invalid request to add an assessment should yield an error.
    Given a request is made to add a question
	  And the isbn of the book the question is about is <isbn>
    And the question text is "<questionText>"
    And the correct answer is "<correctAnswer>"
    #Separate these with commas
    And the incorrect answers will be "<incorrectAnswers>" 
    When the request is made 
    Then an error response will be returned
    And the status code will be 400
    
    #Question text is required
    #Correct answer is required
    #At least one incorrect answer is required
    
    Examples:
    |isbn          |questionText                                                     |correctAnswer                                               |incorrectAnswers                                                                |
    |9999976590739 |                                                                 | Utah                                                       |  New York, Michigan, Ohio                                                      |
    |9999976590739 |Where was Connor born?                                           |                                                            |  New York, Michigan, Ohio                                                      |
    |9999976590739 |Where was Connor born?                                           | Utah                                                       |                                                                                |   
    
    
    #NOTE: The difference between this and the above scenario, is here, all the required fields are filled in.
    Scenario Outline: An invalid request to add a question to a non-existent assessment should yield an error
    Given a request is made to add a question
	  And the isbn of the book the question is about is <isbn>
    And the question text is "<questionText>"
    And the correct answer is "<correctAnswer>"
    #Separate these with commas
    And the incorrect answers will be "<incorrectAnswers>" 
    When the request is made 
    Then an error response will be returned
    And the status code will be 404
    
    Examples:
    |isbn          |questionText                                                     |correctAnswer                                               |incorrectAnswers                                                                |
    |9999976590739 |Where was Connor born?                                           | Utah                                                       |  New York, Michigan, Ohio                                                      |
   
   
   
Scenario Outline: A valid request to add a question to an assessment should succeed.
    Given a request is made to add a question
	  And the isbn of the book the question is about is <isbn>
    And the question text is "<questionText>"
    And the correct answer is "<correctAnswer>"
    #Separate these with commas
    And the incorrect answers will be "<incorrectAnswers>" 
	  When the request is made 
	  Then the question text in the response will be the same as the text in the request
	  Then the correct answer in the response will be the same as the answer in the request
	  And the incorrect answers in the response will be the same as the incorrect answers in the request
	  
	   Examples:
    |isbn            |questionText                                                     |correctAnswer                                               |incorrectAnswers                                                                |
    | 9781976530739  |What was the name of the captain in the novel?                   |Ahab                                                        |John, Herman (after the author Herman Mellevile), Moby                          |
    
    