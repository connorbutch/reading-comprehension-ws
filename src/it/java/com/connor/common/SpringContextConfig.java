package com.connor.common;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.ClassRule;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.MySQLContainer;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import com.connor.Application;
import org.testcontainers.DockerClientFactory;

import io.cucumber.java8.En;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = Application.class, properties = {"spring.main.allow-bean-definition-overriding=true"})
@ContextConfiguration(initializers = SpringContextConfig.DataSourceFromTestContainerInitializer.class)
public class SpringContextConfig implements En {

	@ClassRule
	public static MySQLContainer<?> mysqlDb = getContainer();

	/**
	 * Required to avoid nasty error messages
	 * @return
	 */
	public static MySQLContainer<?> getContainer(){

		//avoid
		/*
		 * /*
		 * java.lang.NullPointerException: null
	at org.testcontainers.DockerClientFactory.dockerHostIpAddress(DockerClientFactory.java:240) ~[testcontainers-1.12.4.jar:na]
	at org.testcontainers.containers.ContainerState.getContainerIpAddress(ContainerState.java:29) ~[testcontainers-1.12.4.jar:na]
	at org.testcontainers.containers.MySQLContainer.getJdbcUrl(MySQLContainer.java:69) ~[mysql-1.12.4.jar:na]
	at SpringContextConfigForCukeUsage$Initializer.initialize(SpringContextConfigForCukeUsage.java:111) ~[test-classes/:na]
	at org.springframework.boot.SpringApplication.applyInitializers(SpringApplication.java:649) ~[spring-boot-2.1.3.RELEASE.jar:2.1.3.RELEASE]
	at org.springframework.boot.SpringApplication.prepareContext(SpringApplication.java:373) ~[spring-boot-2.1.3.RELEASE.jar:2.1.3.RELEASE]
	at org.springframework.boot.SpringApplication.run(SpringApplication.java:314) ~[spring-boot-2.1.3.RELEASE.jar:2.1.3.RELEASE]
	at org.springframework.boot.test.context.SpringBootContextLoader.loadContext(SpringBootContextLoader.java:127) ~[spring-boot-test-2.1.3.RELEASE.jar:2.1.3.RELEASE]
		 */

		DockerClientFactory.instance().client();
		MySQLContainer<?> container = new MySQLContainer<>("mysql:latest")
				.withInitScript("sqlForIntegrationTest.sql");
		container.start();		
		return container;
	}

	public static class DataSourceFromTestContainerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

		@Override
		public void initialize(ConfigurableApplicationContext applicationContext) {
			// TODO Auto-generated method stub
			//add the url, username, and password to the test context before it starts
			TestPropertyValues.of(String.format("spring.datasource.url=%s", mysqlDb.getJdbcUrl()),
					String.format("spring.datasource.username=%s", mysqlDb.getUsername()),
					String.format("spring.datasource.password=%s", mysqlDb.getPassword()),
					String.format("logging.level.root=%s", "ERROR"), //set log level debug
					"spring.main.allow-bean-definition-overriding=true") //TODO see if can do this from within spring boot test annotation
			.applyTo(applicationContext.getEnvironment());	
		}		
	}

	/**
	 * This must contain a "dummy" step definition here to be picked up and startup our context correctly
	 */
	public SpringContextConfig() {
		Given("a", (Integer int1) -> {
			// Write code here that turns the phrase above into concrete actions
			//throw new cucumber.api.PendingException();
		});
	}
}