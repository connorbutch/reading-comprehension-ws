package com.connor.verify;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import com.connor.common.SpringContextConfig;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

/**
 * Runs cucumber tests for verifying an assessment.
 * @author connor
 *
 */
@RunWith(Cucumber.class) //NOTE: cucumber has not yet been integrated with junit 5, but so we will run with junit-vintage-engine.  However, we have lambda support and such from custom imports
@CucumberOptions(features = "src/it/resources/features/VerifyAssessment.feature", plugin = {"json:target/verify-assessment/cucumber.json", "html:target/verify-assessment/html"}, glue = {"com.connor.common", "com.connor.verify"})
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class RunVerifyAssessmentIT {
	//intentionally blank
}
