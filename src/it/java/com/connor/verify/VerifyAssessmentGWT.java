package com.connor.verify;

import static io.restassured.RestAssured.defaultParser;
import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URI;

import org.springframework.boot.web.server.LocalServerPort;

import com.connor.model.dto.ApiErrorDTO;
import com.connor.model.dto.AssessmentDTO;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java8.En;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.ValidatableResponse;

/**
 * Contains the given, when, then step definitions for verifying an assessment
 * @author connor
 *
 */
public class VerifyAssessmentGWT implements En  {
	
	/**
	 * 
	 */
	private static final String URI_FORMAT_STR = "http://localhost:%d/assessments/%d"; 

	/**
	 * 
	 */
	@LocalServerPort
	private int serverPort;

	/**
	 * 
	 */
	private Long isbnPathParameter;
	
	/**
	 * 
	 */
	private Boolean isVerified;
	
	/**
	 * 
	 */
	private ValidatableResponse response;

	/**
	 * 
	 */
	private Scenario scenario;

	/**
	 * Saves scenario for use with scenario.write
	 * @param scenario
	 */
	@Before
	public void init(Scenario scenario) {
		this.scenario = scenario;
		defaultParser = Parser.JSON;
	}	

	/**
	 * Responsible for cleaning up between test cases.
	 */
	@After
	public void cleanup() {
		isVerified = null;
		isbnPathParameter = null;
		response = null;
	}
	
	public VerifyAssessmentGWT() {
		Given("a request is made to verify an assessment", () -> {
		    //intentionally blank -- makes feature file more readable
		});
		
		Given("the isbn of the assessment to verify is {long}", (Long isbn) -> {
		   this.isbnPathParameter = isbn;
		});
		
		Given("the verified value is ", () -> {
			//NOTE: Just here for placehholder for now
		});

		When("the request is made", () -> {
			response = given()
					.body(isVerified)
					.contentType(ContentType.JSON)
					.filter(new RequestLoggingFilter())
					.filter(new ResponseLoggingFilter())
					.patch(getURI())
					.then()
					.contentType(ContentType.JSON);	
		});
		
		Then("an error response will be returned", () -> {
			assertTrue(response
					.extract()
					.as(ApiErrorDTO.class) instanceof ApiErrorDTO, "Response should be instance of api error");
		});

		Then("the status code will be {int}", (Integer expectedHttpStatusCodeInResponse) -> {
			assertEquals(expectedHttpStatusCodeInResponse, response
					.extract()
					.statusCode(), "Status code should match");
		});		
		
		Given("the verified value is {string}", (String verifiedStr) -> {
		    isVerified = (verifiedStr == null)? null: Boolean.parseBoolean(verifiedStr);
		});
		
		Then("the isbn will match the isbn sent above", () -> {
			assertEquals(isbnPathParameter, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getIsbn(), "Isbn returned in response should match the isbn sent in request");
		});
		
		Then("the author's first name will be {string}", (String authorFirstName) -> {
			assertEquals(authorFirstName, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getAuthorFirstName(), "Author first name must match expected value");
		});

		Then("the author's last name will be {string}", (String authorLastName) -> {
			assertEquals(authorLastName, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getAuthorLastName(), "Author last name must match expected value");
		});

		Then("the title will be {string}", (String title) -> {
			assertEquals(title, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getTitle(), "Title in response should be same value passed in request");
		});

		Then("the number of points will be {double}", (Double numberOfPoints) -> {
		   assertEquals(numberOfPoints, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getPoints(), "Number of points must match");
		});
		
		Then("the reading level will be {double}", (Double readingLevel) -> {
		    assertEquals(readingLevel, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getReadingLevel(), "Reading level must match");
		});

		Then("the created teacher id will be {int}", (Integer createdTeacherId) -> {
		    assertEquals(createdTeacherId, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getCreatedTeacherId(), "Created teacher id must match");
		});
		
		Then("the verified value will be the same as the one sent above", () -> {
		   assertEquals(isVerified, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getIsVerified(), "Verification value in response should match value sent in response");
		});
		
		Then("an assessment will be returned", () -> {
			assertTrue(response
			.contentType(ContentType.JSON)
			.extract()
			.as(AssessmentDTO.class) instanceof AssessmentDTO, "Response should be instance of assessment");
		});
	}	
	
	/**
	 * 
	 * @return
	 */
	protected URI getURI() {
		return URI.create(String.format(URI_FORMAT_STR, serverPort, isbnPathParameter));
	}
}