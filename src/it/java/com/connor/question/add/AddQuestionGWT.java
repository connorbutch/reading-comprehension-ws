package com.connor.question.add;

import static io.restassured.RestAssured.defaultParser;
import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URI;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.boot.web.server.LocalServerPort;

import com.connor.model.Question;
import com.connor.model.dto.ApiErrorDTO;
import com.connor.model.dto.AssessmentDTO;
import com.connor.model.dto.QuestionDTO;
import com.connor.model.dto.IncorrectAnswerDTO;
import com.connor.model.IncorrectAnswer;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java8.En;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.ValidatableResponse;

/**
 * 
 * @author connor
 *
 */
public class AddQuestionGWT implements En {

	/**
	 * 
	 */
	private static final String URI_FORMAT_STR = "http://localhost:%d/assessments/%d/questions"; 

	/**
	 * 
	 */
	@LocalServerPort
	private int serverPort;

	/**
	 * 
	 */
	private QuestionDTO question;

	/**
	 * 
	 */
	private Long isbnPathParam;

	/**
	 * 
	 */
	private ValidatableResponse response;

	/**
	 * 
	 */
	private Scenario scenario;

	/**
	 * Saves scenario for use with scenario.write
	 * @param scenario
	 */
	@Before
	public void init(Scenario scenario) {
		this.scenario = scenario;
		defaultParser = Parser.JSON;
	}	

	/**
	 * Responsible for cleaning up between test cases.
	 */
	@After
	public void cleanup() {
		this.isbnPathParam = null;
		this.response = null;
		this.question = null;
	}	

	public AddQuestionGWT() {
		Given("a request is made to add a question", () -> {
			//intentionally blank
		});		

		Given("the question text is {string}", (String questionText) -> {
			initializeQuestionIfNeeded();
			question.setQuestionText(questionText);
		});		

		Given("the correct answer is {string}", (String correctAnswer) -> {
			initializeQuestionIfNeeded();
			question.setCorrectAnswer(correctAnswer);
		});	

		Given("the incorrect answers will be {string}", (String incorrectAnswers) -> { //TODO custom transformer here; just split on our own for mvp/mmp
			initializeQuestionIfNeeded();
			if(StringUtils.isNotBlank(incorrectAnswers)) {
				question.setIncorrectAnswers(
						Arrays.stream(incorrectAnswers.split(","))
						.map(incorrectText -> {
							IncorrectAnswerDTO incorrectAnswer = new IncorrectAnswerDTO();
							incorrectAnswer.setAnswerText(incorrectText);
							return incorrectAnswer;
						})
						.collect(Collectors.toList())
				);				
			}
		});	

		When("the request is made", () -> {
			response = given()
					.body(question)
					.contentType(ContentType.JSON)
					.filter(new RequestLoggingFilter())
					.filter(new ResponseLoggingFilter())
					.post(getURI())
					.then()
					.contentType(ContentType.JSON);	
		});	

		Then("an error response will be returned", () -> {
			assertTrue(response
					.extract()
					.as(ApiErrorDTO.class) instanceof ApiErrorDTO, "Response should be instance of api error");
		});	

		Then("the status code will be {int}", (Integer expectedHttpStatusCode) -> {
			assertEquals(expectedHttpStatusCode, response
					.extract()
					.statusCode(), "Status code should match");
		});		

		Given("the isbn of the book the question is about is {long}", (Long isbn) -> {
			isbnPathParam = isbn;			
		});			

		Then("the question text in the response will be the same as the text in the request", () -> {
			assertEquals(question.getQuestionText(), response
					.contentType(ContentType.JSON)
					.extract()
					.as(QuestionDTO.class).getQuestionText(), "Question text in response should be same value passed in request");
		});		

		Then("the correct answer in the response will be the same as the answer in the request", () -> {
			assertEquals(question.getCorrectAnswer(), response
					.contentType(ContentType.JSON)
					.extract()
					.as(QuestionDTO.class).getCorrectAnswer(), "Correct answer in response should be same value passed in request");
		});		

		Then("the incorrect answers in the response will be the same as the incorrect answers in the request", () -> {
			if(CollectionUtils.isNotEmpty(question.getIncorrectAnswers())) {
				List<IncorrectAnswerDTO> incorrectAnswersFromResponse = response
				.contentType(ContentType.JSON)
				.extract()
				.as(QuestionDTO.class).getIncorrectAnswers();
				assertTrue(CollectionUtils.isNotEmpty(incorrectAnswersFromResponse), "Response should contain a list of incorrect answers");
				assertEquals(question.getIncorrectAnswers().size(), incorrectAnswersFromResponse.size(), "The number of incorrect answers in the response should match the number sent in the request");
				
				//now check each incorrect answer is present
				question.getIncorrectAnswers().forEach(incorrectAnswer -> {
					boolean didFindMatchingAnswer = false;
					for(IncorrectAnswerDTO incorrectDTO: incorrectAnswersFromResponse) {
						if(incorrectDTO.getAnswerText().trim().equals(incorrectAnswer.getAnswerText().trim())) {
							didFindMatchingAnswer = true;
							break;
						}
					}
					
					assertTrue(didFindMatchingAnswer, "We should find a matching incorrect answer in response");
				});
			}			
		});
	}	

	/**
	 * 
	 */
	protected void initializeQuestionIfNeeded() {
		if(question == null) {
			question = new QuestionDTO();
		}
	}

	/**
	 * 
	 * @return
	 */
	protected URI getURI() {
		return URI.create(String.format(URI_FORMAT_STR, serverPort, isbnPathParam));
	}
}