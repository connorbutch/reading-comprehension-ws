package com.connor.question.add;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

/**
 * 
 * @author connor
 *
 */
@RunWith(Cucumber.class) //NOTE: cucumber has not yet been integrated with junit 5, but so we will run with junit-vintage-engine.  However, we have lambda support and such from custom imports
@CucumberOptions(features = "src/it/resources/features/AddQuestionForAssessment.feature", plugin = {"json:target/add-question/cucumber.json", "html:target/add-question/html"}, glue = {"com.connor.common", "com.connor.question.add"})
public class RunAddQuestionIT {
	//intentionally blank
}
