package com.connor.health;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class) //NOTE: cucumber has not yet been integrated with junit 5, but so we will run with junit-vintage-engine.  However, we have lambda support and such from custom imports
@CucumberOptions(features = "src/it/resources/features/HealthCheck.feature", plugin = {"json:target/health-check/cucumber.json", "html:target/health-check/html"}, glue = {"com.connor.common", "com.connor.health"})
public class RunHealthCheckIT {
	//intentionally blank
}
