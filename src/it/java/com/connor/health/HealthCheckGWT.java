package com.connor.health;
import static org.junit.jupiter.api.Assertions.*;

import java.net.URI;

import org.junit.ClassRule;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.testcontainers.containers.MySQLContainer;

import io.cucumber.java8.En;

/**
 * 
 * @author connor
 *
 */
//@DirtiesContext //TODO verify this is correct usage
public class HealthCheckGWT implements En {

	/**
	 * 
	 */
	private static final String HEALTH_URI_FORMAT_STR = "http://localhost:%d/health";
	
	/**
	 * 
	 */
	@LocalServerPort
	private int serverPort;

	/**
	 * 
	 */
	private final TestRestTemplate testRestTemplate = new TestRestTemplate();

	/**
	 * 
	 */
	private ResponseEntity<?> response;

	public HealthCheckGWT() {
		Given("The server is started", () -> {
			//intentionally blank -- this is just a placeholder for readability of feature file
		});


		When("a health check request is made", () -> {
			response = testRestTemplate.getForEntity(getURI(), String.class);	
		});


		Then("the response will indicate the server is healthy", () -> {
			assertEquals(response.getStatusCode(), HttpStatus.OK, "Should get a 200 response on a health check");
		});
	}

	/**
	 * 
	 * @return
	 */
	protected URI getURI() {
		String uriStr = String.format(HEALTH_URI_FORMAT_STR, serverPort);
		return URI.create(uriStr);
	}
}