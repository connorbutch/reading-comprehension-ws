package com.connor.retrieve;

import static io.restassured.RestAssured.*;
import static org.junit.jupiter.api.Assertions.*;

import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Links;
import org.springframework.http.ResponseEntity;

import org.springframework.test.annotation.DirtiesContext;
import org.springframework.web.client.RestClientException;

import com.connor.model.dto.ApiErrorDTO;
import com.connor.model.dto.AssessmentDTO;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.time.Duration;

import org.slf4j.Logger;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java8.En;
import io.micrometer.core.instrument.util.StringUtils;
import io.cucumber.java.Scenario;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.response.ValidatableResponse;
import io.restassured.http.ContentType;


/**
 * This class holds the glue (given, when then) step definitions for getting an assesment.
 * @author connor
 *
 */
public class GetAssessmentIsbnGWT implements En {

	/**
	 * Use with String.format and isbn to get path
	 */
	private static final String GET_URI_FORMAT_STR = "http://localhost:%d/assessments/%d"; 

	/**
	 * 
	 */
	private static final Duration DEFAULT_TIMEOUT_DURATION = Duration.ofMillis(5000);

	/**
	 * 
	 */
	private static final Logger log = LoggerFactory.getLogger(GetAssessmentIsbnGWT.class);

	/**
	 * 
	 */
	@LocalServerPort
	private int serverPort;

	/**
	 * isbn used as path parameter
	 */
	private Long isbn;

	/**
	 * 
	 */
	private ValidatableResponse response;

	/**
	 * 
	 */
	private Scenario scenario;

	/**
	 * 
	 * @param scenario
	 */
	@Before
	public void init(Scenario scenario) {
		this.scenario = scenario;
		defaultParser = Parser.JSON;
	}	

	/**
	 * 
	 */
	@After
	public void cleanup() {
		isbn = null;
		response = null;
	}

	public GetAssessmentIsbnGWT()  {
		Given("a request is made to retrieve assessment by isbn", () -> {
			//intentionally blank -- just here for readability in gherkin
		});		

		Given("the isbn searched on is {long}", (Long isbn) -> {
			this.isbn = isbn;
		});		

		When("the request is made", () -> {
			response = given()
					.filter(new RequestLoggingFilter())
					.filter(new ResponseLoggingFilter())
					.get(getURI())
					.then()
					.contentType(ContentType.JSON);		
		});	

		Then("an error response will be returned", () -> {
			assertTrue(response
					.contentType(ContentType.JSON)
					.extract()
					.as(ApiErrorDTO.class) instanceof ApiErrorDTO, "Response should be an api error");
		});

		Then("the status code will be {int}", (Integer httpStatusCodeFromResponse) -> {
			assertEquals(httpStatusCodeFromResponse, response
					.extract()
					.statusCode(), "Status code should match");
		});

		Then("an assessment will be returned", () -> {
			assertTrue(response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class) instanceof AssessmentDTO, "Response should be an AssessmentDTO");
		});

		Then("the isbn will match the isbn sent above", () -> {
			assertEquals(isbn, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getIsbn(), "Isbn should match expected value from feature file");
		});

		Then("the author's first name will be {string}", (String authorFirstName) -> {
			assertEquals(authorFirstName, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getAuthorFirstName(), "Author first name should match expected value from feature file");
		});		

		Then("the author's last name will be {string}", (String authorLastName) -> {
			assertEquals(authorLastName, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getAuthorLastName(), "Author last name should match expected value from feature file");
		});

		Then("the title will be {string}", (String title) -> {
			assertEquals(title, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getTitle(), "Title should match expected value from feature file");		 
		});

		Then("the number of points will be {double}", (Double numberOfPoints) -> {
			assertEquals(numberOfPoints, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getPoints(), "Number of points should match expected value from feature file");
		});

		Then("the reading level will be {double}", (Double readingLevel) -> {
			assertEquals(readingLevel,response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getReadingLevel(), "Reading level should match expected value from feature file");
		});

		Then("the verification value will be {string}", (String expectedVerificationValue) -> { //TODO add custom converter type here
			boolean expectedValue = Boolean.parseBoolean(expectedVerificationValue);
			assertEquals(expectedValue, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getIsVerified(), "Verification value should match");
		});

		Then("the created teacher id will be {int}", (Integer createdTeacherId) -> {
			assertEquals(createdTeacherId,response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getCreatedTeacherId(),"Created teacher id should match expected value from feature file");
		});		

		Then("the href link will end with {string}", (String href) -> {
			//TODO
//			boolean foundLink = false;
//			AssessmentDTO assessmentResponse = response
//					.contentType(ContentType.JSON)
//					.extract()
//					.as(AssessmentDTO.class);
//			if(StringUtils.isNotBlank(href)) {
//				for(Link link: assessmentResponse.getLinks()) {
//					if(link.getHref() != null && link.getHref().trim().endsWith((href.trim()))) {
//						foundLink = true;
//						break;
//					}
//				}
//			}else {
//				foundLink = true;
//			}
//			
//			assertTrue(foundLink, "Should find expected hateaos link");
		});		
	}	

	/**
	 * Get uri for use in request
	 * @return
	 */
	protected URI getURI() {
		String uriStr = String.format(GET_URI_FORMAT_STR, serverPort, isbn);
		return URI.create(uriStr);
	}

	/**
	 * 
	 * @return
	 */
	protected Duration getTimeout() {
		return DEFAULT_TIMEOUT_DURATION;
	}
}