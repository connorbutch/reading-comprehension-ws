package com.connor.retrieve;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

/**
 * This is the runner class for running the get assessment by isbn integration tests.
 * @author connor
 *
 */
@RunWith(Cucumber.class) //NOTE: cucumber has not yet been integrated with junit 5, but so we will run with junit-vintage-engine.  However, we have lambda support and such from custom imports
@CucumberOptions(features = "src/it/resources/features/GetAssessmentForIsbn.feature", plugin = {"json:target/get-assessment/cucumber.json", "html:target/get-assessment/html"}, glue = {"com.connor.common", "com.connor.retrieve"})
public class RunGetAssessmentIsbnIT {
	//intentionally blank -- runner class
}
