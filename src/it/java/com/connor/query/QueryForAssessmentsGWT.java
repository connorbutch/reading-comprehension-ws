package com.connor.query;

import static io.restassured.RestAssured.defaultParser;
import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URI;

import org.springframework.boot.web.server.LocalServerPort;

import com.connor.model.dto.ApiErrorDTO;
import com.connor.model.dto.AssessmentDTO;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java8.En;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.ValidatableResponse;

import java.util.List;

/**
 * This class holds the "given, when then" step definitions for querying for multiple assessments.
 * @author connor
 *
 */
public class QueryForAssessmentsGWT implements En {

	/**
	 * 
	 */
	private static final String URI_FORMAT_STR = "http://localhost:%d/assessments"; 

	/**
	 * 
	 */
	@LocalServerPort
	private int serverPort;

	/**
	 * 
	 */
	private Scenario scenario;

	/**
	 * 
	 */
	private Integer limit;

	/**
	 * 
	 */
	private Integer offset;

	/**
	 * 
	 */
	private Integer createdTeacherId;

	/**
	 * 
	 */
	private ValidatableResponse response;

	/**
	 * Saves scenario for use with scenario.write
	 * @param scenario
	 */
	@Before
	public void init(Scenario scenario) {
		this.scenario = scenario;
		defaultParser = Parser.JSON;
	}	

	/**
	 * Responsible for cleaning up between test cases.
	 */
	@After
	public void cleanup() {
		this.limit = null;
		this.offset = null;
		this.createdTeacherId = null;
		this.response = null;
	}

	/**
	 * Holds gwt steps in constructor
	 */
	@SuppressWarnings("unchecked") //use for casting List<?> to List<T>
	public QueryForAssessmentsGWT() {
		Given("a request is made to retrieve assessments", () -> {
			//intentionally blank
		});

		Given("the limit passed is {int}", (Integer limit) -> {
			this.limit = limit;
		});

		Given("the offset passed is {int}", (Integer offset) -> {
			this.offset = offset;
		});

		Given("the created teacher id is {int}", (Integer createdTeacherId) -> {
			this.createdTeacherId = createdTeacherId;
		});

		When("the request is made", () -> {
			response = given()
					.contentType(ContentType.JSON)
					.filter(new RequestLoggingFilter())
					.filter(new ResponseLoggingFilter())
					.get(getURI())
					.then()
					.contentType(ContentType.JSON);	
		});

		Then("an error response will be returned", () -> {
			assertTrue(response
					.extract()
					.as(ApiErrorDTO.class) instanceof ApiErrorDTO, "Response should be instance of api error");
		});

		Then("the status code will be {int}", (Integer httpStatusCode) -> {			
			assertEquals(httpStatusCode, response
					.extract()
					.statusCode(), "Http status code should match");
		});

		Then("an empty list will be returned", () -> {
			List<?> list = response
					.extract()
					.as(List.class);
			assertEquals(list.size(), 0, "List should be empty");
		});

		Then("the total number of assessment header will be {int}", (Integer totalNumberOfAssessmentsHeader) -> {
			response
			.assertThat()
			.header("totalNumberOfAssessments", String.valueOf(totalNumberOfAssessmentsHeader));
		});

		Then("a non-empty list will be returned", () -> {
			List<?> list = response
					.extract()
					.as(List.class);
			assertTrue(list != null, "List should not be null");
			assertTrue(!list.isEmpty(), "List should not be empty");
		});

		Then("one of the assessments will be for the book with title {string}", (String title) -> {
			assertTrue(((List<AssessmentDTO>)response
					.extract()
					.as(List.class))
					.stream()
					.filter(assessment -> {
						return assessment != null && title.equalsIgnoreCase(assessment.getTitle());
					})
					.findFirst()
					.isPresent(), "There should be an assessment for a book with a matching title");
		});

		Then("the number of assessments returned will be {int}", (Integer numberOfAssessments) -> {
			assertEquals(response
					.extract()
					.as(List.class).size(), numberOfAssessments, "Number of assessments should match");
		});
	}	

	protected URI getURI() {
		StringBuilder sb = new StringBuilder(String.format(URI_FORMAT_STR, serverPort));

		//use this to know if we need to append ? or & before parameter
		boolean hasAtLeastOneQueryParam = false;

		if(limit != null) {
			sb = addSeparator(sb, hasAtLeastOneQueryParam);
			sb.append("limit=");
			sb.append(limit);
			hasAtLeastOneQueryParam = true;
		}

		if(offset != null) {
			sb = addSeparator(sb, hasAtLeastOneQueryParam);
			sb.append("offset=");
			sb.append(offset);
			hasAtLeastOneQueryParam = true;
		}

		if(createdTeacherId != null) {
			sb = addSeparator(sb, hasAtLeastOneQueryParam);
			sb.append("createdTeacherId=");
			sb.append(createdTeacherId);
			hasAtLeastOneQueryParam = true;
		}

		return URI.create(sb.toString());
	}	
	
	/**
	 * 
	 * @param sb
	 * @param hasAtLeastOneQueryParam
	 * @return
	 */
	protected StringBuilder addSeparator(StringBuilder sb, boolean hasAtLeastOneQueryParam) {
		StringBuilder separator = (hasAtLeastOneQueryParam)? new StringBuilder("&") : new StringBuilder("?"); 
		return new StringBuilder(sb)
				.append(separator);
	}
}