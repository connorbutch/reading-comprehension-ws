package com.connor.add;

import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class) //NOTE: cucumber has not yet been integrated with junit 5, but so we will run with junit-vintage-engine.  However, we have lambda support and such from custom imports
@CucumberOptions(features = "src/it/resources/features/AddAssessmentForIsbn.feature", plugin = {"json:target/add-assessment/cucumber.json", "html:target/add-assessment/html"}, glue = {"com.connor.common", "com.connor.add"})
public class RunAddAssessmentIT {

}
