package com.connor.add;

import static io.restassured.RestAssured.defaultParser;
import static io.restassured.RestAssured.*;
import static org.junit.jupiter.api.Assertions.*;

import java.net.URI;

import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;

import com.connor.model.Assessment;
import com.connor.model.dto.ApiErrorDTO;
import com.connor.model.dto.AssessmentDTO;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java8.En;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.ValidatableResponse;

/**
 * 
 * @author connor
 *
 */
public class AddAssessmentGWT implements En {

	/**
	 * 
	 */
	private static final String URI_FORMAT_STR = "http://localhost:%d/assessments/%d"; 

	/**
	 * 
	 */
	@LocalServerPort
	private int serverPort;

	/**
	 * 
	 */
	private AssessmentDTO assessment;

	/**
	 * 
	 */
	private Long isbnPathParam;

	/**
	 * 
	 */
	private ValidatableResponse response;

	/**
	 * 
	 */
	private Scenario scenario;

	/**
	 * Saves scenario for use with scenario.write
	 * @param scenario
	 */
	@Before
	public void init(Scenario scenario) {
		this.scenario = scenario;
		defaultParser = Parser.JSON;
	}	

	/**
	 * Responsible for cleaning up between test cases.
	 */
	@After
	public void cleanup() {
		assessment = null;
		isbnPathParam = null;
		response = null;
	}

	public AddAssessmentGWT() {
		Given("a request is made to add an assessment", () -> {
			//intentionally blank -- just here for readability 
		});	

		Given("the isbn to be added \\(path parameter) is {long}", (Long isbnPathParam) -> {
			this.isbnPathParam = isbnPathParam;
		});		

		Given("the isbn in the body is {long}", (Long isbn) -> {
			initializeRequestIfNeeded();
			assessment.setIsbn(isbn);
		});	

		Given("the author first name is {string}", (String firstName) -> {
			initializeRequestIfNeeded();
			assessment.setAuthorFirstName(firstName);
		});

		Given("the author last name is {string}", (String lastName) -> {
			initializeRequestIfNeeded();
			assessment.setAuthorLastName(lastName);
		});

		Given("the title is {string}", (String title) -> {
			initializeRequestIfNeeded();
			assessment.setTitle(title);
		});

		Given("the reading level is {double}", (Double readingLevel) -> {
			initializeRequestIfNeeded();
			assessment.setReadingLevel(readingLevel);
		});

		Given("the number of  points is {double}", (Double points) -> {
			initializeRequestIfNeeded();
			assessment.setPoints(points);
		});

		Given("the created teacher id is {int}", (Integer createdTeacherId) -> {
			initializeRequestIfNeeded();
			assessment.setCreatedTeacherId(createdTeacherId);
		});

		Given("the test verified value is {string}", (String verifiedStr) -> {
			initializeRequestIfNeeded();
			assessment.setIsVerified(Boolean.parseBoolean(verifiedStr.trim()));
		});

		When("the request is made", () -> {
			response = given()
					.body(assessment)
					.contentType(ContentType.JSON)
					.filter(new RequestLoggingFilter())
					.filter(new ResponseLoggingFilter())
					.post(getURI())
					.then()
					.contentType(ContentType.JSON);		
		});	

		Then("an error response will be returned", () -> {
			assertTrue(response
					.extract()
					.as(ApiErrorDTO.class) instanceof ApiErrorDTO, "Response should be instance of api error");
		});

		Then("the status code will be {int}", (Integer expectedHttpStatusCodeInResponse) -> {
			assertEquals(expectedHttpStatusCodeInResponse, response
					.extract()
					.statusCode(), "Status code should match");
		});

		Then("a successful response will be returned", () -> {
			assertTrue(response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class) instanceof AssessmentDTO, "Response should be an AssessmentDTO");
		});		

		Then("the isbn returned will be the same as one passed", () -> {
			assertEquals(isbnPathParam, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getIsbn(), "Isbn returned in response should match the isbn sent in request");
		});

		Then("the verified value will be false", () -> {
			assertEquals(false, response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getIsVerified(), "Test should not be verified upon creation");
		});

		Then("the author first name will be the same as one passed", () -> {
			assertEquals(assessment.getAuthorFirstName(), response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getAuthorFirstName(), "Author first name in response should be same value passed in request");
		});

		Then("the author last name will be the same as one passed", () -> {
			assertEquals(assessment.getAuthorLastName(), response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getAuthorLastName(), "Author last name in response should be same value passed in request");
		});

		Then("the title will be the same as one passed", () -> {
			assertEquals(assessment.getTitle(), response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getTitle(), "Title in response should be same value passed in request");
		});

		Then("the reading level will be the same as one passed", () -> {
			assertEquals(assessment.getReadingLevel(), response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getReadingLevel(), "Reading level in response should be same value passed in request");
		});

		Then("the number of points will be the same as one passed", () -> {
			assertEquals(assessment.getPoints(), response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getPoints(), "Number of points in response should be same value passed in request");
		});

		Then("the created teacher id will be the same as one passed", () -> {
			assertEquals(assessment.getCreatedTeacherId(), response
					.contentType(ContentType.JSON)
					.extract()
					.as(AssessmentDTO.class).getCreatedTeacherId(), "Created teacher id in response should be same value passed in request");
		});		
		//---------------------------------------------------------------------------------------------------------------------------
		//NOTE: these exist as a short term-solution for use with null/empty cells in examples table.  These will go away when we implement our own nullable types with type register configurer

		Given("the isbn to be added \\(path parameter) is ", () -> {
			//intentionally blank -- this should do nothing, as value should already be null
		});

		Given("the reading level is ", () -> {
			//intentionally blank -- this should do nothing, as value should already be null
		});

		Given("the number of  points is ", () -> {
			//intentionally blank -- this should do nothing, as value should already be null
		});

		Given("the created teacher id is ", () -> {
			//intentionally blank -- this should do nothing, as value should already be null
		});
		//---------------------------------------------------------------------------------------------------------------------------
	}

	/**
	 * 
	 * @return
	 */
	protected URI getURI() {
		return URI.create(String.format(URI_FORMAT_STR, serverPort, isbnPathParam));
	}

	/**
	 * 
	 */
	protected void initializeRequestIfNeeded() {
		if(assessment == null) {
			assessment = new AssessmentDTO();
		}
	}
}