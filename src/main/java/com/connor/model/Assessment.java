package com.connor.model;

import java.io.Serializable;
import java.util.List;

/**
 * Class respresenting an assessment
 * @author connor
 *
 */
public class Assessment implements Serializable {


	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -4286430732191416228L;

	//@NonNull
	/**
	 * Isbn number
	 */
	private Long isbn;

	/**
	 * 
	 */
	private String authorFirstName;
	
	/**
	 * 
	 */
	//@NonNull
	private String title;

	/**
	 * 
	 */
	private String authorLastName;

	/**
	 * 
	 */
	private List<Question> questions;

	/**
	 * 
	 */
	private Double points;

	/**
	 * 
	 */
	private Double readingLevel;
	
	/**
	 * 
	 */
	private boolean isVerified;
	
	/**
	 * 
	 */
	private Integer createdTeacherId;
	
	//TODO once done modifying these definitions, can replace with project lombok, but version sprign boot uses doesn't work with this eclipse version easily
	public Long getIsbn() {
		return isbn;
	}

	public void setIsbn(Long isbn) {
		this.isbn = isbn;
	}

	public String getAuthorFirstName() {
		return authorFirstName;
	}

	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthorLastName() {
		return authorLastName;
	}

	public void setAuthorLastName(String authorLastName) {
		this.authorLastName = authorLastName;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public Double getPoints() {
		return points;
	}	

	public Double getReadingLevel() {
		return readingLevel;
	}

	public void setReadingLevel(Double readingLevel) {
		this.readingLevel = readingLevel;
	}

	public boolean isVerified() {
		return isVerified;
	}

	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public Integer getCreatedTeacherId() {
		return createdTeacherId;
	}

	public void setCreatedTeacherId(Integer createdTeacherId) {
		this.createdTeacherId = createdTeacherId;
	}

	public Double getNumberOfPoints() {
		return points;
	}

	public void setNumberOfPoints(Double points) {
		this.points = points;
	}

	@Override
	public String toString() {
		return "Assessment [isbn=" + isbn + ", authorFirstName=" + authorFirstName + ", title=" + title
				+ ", authorLastName=" + authorLastName + ", questions=" + questions + ", points=" + points
				+ ", readingLevel=" + readingLevel + ", isVerified=" + isVerified + ", createdTeacherId="
				+ createdTeacherId + "]";
	}	
}