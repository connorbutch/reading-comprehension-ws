package com.connor.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;


/**
 * Model class for a question in a reading test.
 * @author Connors
 *
 */

public class Question implements Serializable {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = 790466553613026293L;
	
	/**
	 * 
	 */
	//@NonNull
	private Long questionId;
	
	/**
	 * 
	 */
	//@NonNull
	private Long assessmentIsbn;
	
	/**
	 * 
	 */
	//@NonNull
	private String questionText;
	
	/**
	 * 
	 */
	private String correctAnswer;
	
	/**
	 * 
	 */
	private List<IncorrectAnswer> incorrectAnswers;

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public Long getAssessmentIsbn() {
		return assessmentIsbn;
	}

	public void setAssessmentIsbn(Long assessmentIsbn) {
		this.assessmentIsbn = assessmentIsbn;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public String getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public List<IncorrectAnswer> getIncorrectAnswers() {
		return incorrectAnswers;
	}

	public void setIncorrectAnswers(List<IncorrectAnswer> incorrectAnswers) {
		this.incorrectAnswers = incorrectAnswers;
	}
}