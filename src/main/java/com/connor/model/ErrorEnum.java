package com.connor.model;

import java.util.Arrays;
import java.util.Optional;

/**
 * Enum to hold error messages
 * @author connor
 *
 */
public enum ErrorEnum {	

	/**
	 * Default
	 */
	UNKNOWN("Unknown error", 0);
	//add other values here as needed
	
	/**
	 * 
	 */
	private static final String IS_REQUIRED = " is required";

	/**
	 * 
	 */
	private static final String IS_TOO_LARGE = " exceeds the maximum allowed value";
	
	/**
	 * 
	 */
	private static final String IS_TOO_SMALL = " below the minimum allowed value";
	
	/**
	 * 
	 */
	private final String message;
	
	/**
	 * 
	 */
	private final int errorCode;
	
	/**
	 * Allows to get an error enum by value
	 * @param code
	 * @return
	 */
	public static Optional<ErrorEnum> getEnumFromCode(int code) {
		ErrorEnum errorEnum = null;
		
		for(ErrorEnum errorEnumFromLoop: ErrorEnum.values()) {
			if(errorEnumFromLoop.getErrorCode() == code) {
				errorEnum = errorEnumFromLoop;
				break;
			}
		}
		
		return Optional.ofNullable(errorEnum);
	}
	
	/**
	 * 
	 * @param message
	 * @param errorCode
	 */
	private ErrorEnum(String message, int errorCode) {
		this.message = message;
		this.errorCode = errorCode;
	}

	/**
	 * Getter
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Getter
	 * @return
	 */
	public int getErrorCode() {
		return errorCode;
	}	
}