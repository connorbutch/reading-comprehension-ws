package com.connor.model;

import java.io.Serializable;
import java.time.Instant;

/**
 * This is the response body for any error that occurs in the api (either 4xx or 5xx status codes)
 * @author connor
 *
 */
public class ApiError implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3102128789153137657L;

	/**
	 * 
	 */
	private final String message;

	/**
	 * 
	 */
	private final Instant instant;

	/**
	 * 
	 * @param message
	 * @param instant
	 */
	public ApiError(String message, Instant instant) {
		this.message = message;
		this.instant = instant;
	}

	/**
	 * 
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * 
	 * @return
	 */
	public Instant getInstant() {
		return instant;
	}
}