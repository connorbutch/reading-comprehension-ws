package com.connor.model.dto;

import java.io.Serializable;
import java.time.Instant;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author connor
 *
 */
//NOTE: the following line removes this error: com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException: Unrecognized field "_links" (class com.connor.model.dto.AssessmentDTO), not marked as ignorable
//This also makes it more "future proof", as this allows us to add fields without breaking existing functionality
@JsonIgnoreProperties(ignoreUnknown = true) 
public class ApiErrorDTO extends RepresentationModel<ApiErrorDTO> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7394953403420399307L;
	//NOTE: I am not 100% sure we need to make this support hateaos, but just in case, we do . . . .

	/**
	 * 
	 */
	private final String message;

	/**
	 * 
	 */
	private final Instant instant;

	/**
	 * 
	 * @param message
	 * @param instant
	 */
	public ApiErrorDTO(String message, Instant instant) {
		this.message = message;
		this.instant = instant;
	}

	public String getMessage() {
		return message;
	}

	public Instant getInstant() {
		return instant;
	}	
}