package com.connor.model.dto;

import java.io.Serializable;
import java.util.List;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author connor
 *
 */
//NOTE: the following line removes this error: com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException: Unrecognized field "_links" (class com.connor.model.dto.AssessmentDTO), not marked as ignorable
//This also makes it more "future proof", as this allows us to add fields without breaking existing functionality
@JsonIgnoreProperties(ignoreUnknown = true) 
public class QuestionDTO extends RepresentationModel<QuestionDTO> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6223820017305085983L;

	//NOTE: hateaos allows us to not need to maintain own id (and no need to maintain fk id)

	/**
	 * 
	 */
	private String questionText;

	/**
	 * 
	 */
	private String correctAnswer;

	/**
	 * 
	 */
	private List<IncorrectAnswerDTO> incorrectAnswers;

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public String getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public List<IncorrectAnswerDTO> getIncorrectAnswers() {
		return incorrectAnswers;
	}

	public void setIncorrectAnswers(List<IncorrectAnswerDTO> incorrectAnswers) {
		this.incorrectAnswers = incorrectAnswers;
	}	
}