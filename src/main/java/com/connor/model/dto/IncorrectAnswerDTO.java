package com.connor.model.dto;

import java.io.Serializable;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author connor
 *
 */
//NOTE: the following line removes this error: com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException: Unrecognized field "_links" (class com.connor.model.dto.AssessmentDTO), not marked as ignorable
//This also makes it more "future proof", as this allows us to add fields without breaking existing functionality
@JsonIgnoreProperties(ignoreUnknown = true) 
public class IncorrectAnswerDTO extends RepresentationModel<IncorrectAnswerDTO> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8736514179199164392L;
	
	//NOTE: the existence of hateoas self href means we shouldn't maintain (db) id here
	
	/**
	 * 
	 */
	private String answerText;	

	public String getAnswerText() {
		return answerText;
	}

	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}
}