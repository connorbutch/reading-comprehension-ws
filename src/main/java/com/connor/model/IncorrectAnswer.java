package com.connor.model;

import java.io.Serializable;


/**
 * 
 * @author connor
 *
 */

public class IncorrectAnswer implements Serializable {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -1589995658248423265L;

	
	/**
	 * 
	 */
	private long answerId;
	
	/**
	 * 
	 */
	private long questionId;
	
	/**
	 * 
	 */
	private String answerText;
	
	

	public long getAnswerId() {
		return answerId;
	}

	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

	public String getAnswerText() {
		return answerText;
	}

	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}	
}