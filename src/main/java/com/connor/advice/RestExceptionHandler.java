package com.connor.advice;


import java.time.Instant;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.connor.exception.AssessmentNotFoundException;
import com.connor.exception.DuplicateResourceException;
import com.connor.exception.InvalidRequestException;
import com.connor.exception.ReadingSystemException;
import com.connor.model.ApiError;
import com.connor.model.dto.ApiErrorDTO;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

/**
 * This class handles the horizontal concern of getting exceptions and marshalling them to a response.
 * @author connor
 *
 */
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
	
	/**
	 * 
	 */
	private static final String UNEXPECTED_ERROR_MSG = "An unexpected error occurred";
	
	

	/**
	 * 
	 */
	private final Logger log;
	
	/**
	 * 
	 * @param log
	 */
	@Autowired
	public RestExceptionHandler(Logger log) {
		this.log = log;
	}
	
	/**
	 * Handle invalid request, returning a response with a 400 status code.
	 * @param e
	 * @return
	 */
	@ExceptionHandler(InvalidRequestException.class)
	public ResponseEntity<ApiErrorDTO> handleInvalidRequest(InvalidRequestException e){ 
		log.error("We recieved an invalid request", e);		
		ApiErrorDTO responseBody = new ApiErrorDTO(e.getMessage(), Instant.now());	
		return ResponseEntity.badRequest().body(responseBody);
	}
	
	/**
	 * Handles request for uri that does not exist by returning 404 status code.
	 * @param e
	 * @return
	 */
	@ExceptionHandler(AssessmentNotFoundException.class)
	public ResponseEntity<ApiErrorDTO> handleNotFound(AssessmentNotFoundException e){
		log.error("We recieved a request for an assessment that is not found", e);
		ApiErrorDTO responseBody = new ApiErrorDTO(e.getMessage(), Instant.now()); 		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBody);
	}
	
	/**
	 * Handles system exceptions by returning a 500 status code to client.
	 * @param e
	 * @return
	 */
	@ExceptionHandler(ReadingSystemException.class)
	public ResponseEntity<ApiErrorDTO> handleSystemException(ReadingSystemException e){
		log.error("An unexpected system exception occurred.  This should be looked at immediately", e);
		ApiErrorDTO responseBody = new ApiErrorDTO(UNEXPECTED_ERROR_MSG, Instant.now());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseBody);
	}
	
	/**
	 * Handles a duplicate resource exception by returning a 409 to the client.
	 * @param e
	 * @return
	 */
	@ExceptionHandler(DuplicateResourceException.class)
	public ResponseEntity<ApiErrorDTO> handleDuplicateResourceException(DuplicateResourceException e){
		log.error("A duplicate resource exception occurred", e);
		ApiErrorDTO responseBody = new ApiErrorDTO("A duplicate resource exception occurred", Instant.now());
		return ResponseEntity.status(HttpStatus.CONFLICT).body(responseBody);
	}
	
	/**
	 * This provides error handling for all uncaught exceptions
	 */
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(
			Exception e, @Nullable Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
		log.error("An unexpected system exception occurred.  This should be looked at immediately", e);
		ApiError responseBody = new ApiError(UNEXPECTED_ERROR_MSG, Instant.now());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseBody);		
	}
}