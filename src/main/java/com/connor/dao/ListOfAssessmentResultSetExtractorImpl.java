package com.connor.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.connor.model.Assessment;
import com.connor.model.Question;

/**
 * This class is needed for extracting more than one assessment from a result set
 * @author connor
 *
 */
@Component
public class ListOfAssessmentResultSetExtractorImpl extends AbstractBaseResultSetExtractor implements ResultSetExtractor<List<Assessment>> {

	
	/**
	 * Cdi enabled constructor
	 * @param log
	 */
	@Autowired
	public ListOfAssessmentResultSetExtractorImpl(Logger log) {
		super(log);
	}


	@Override
	public List<Assessment> extractData(ResultSet rs) throws SQLException, DataAccessException {
		log.debug("Inside ListOfAssessmentResultSetExtractorImpl.extractData");
		List<Assessment> assessments = new ArrayList<>();
		
		//keep track of assessments/questions tied to their respective primary keys
		Map<Long, Assessment> assessmentsWithPrimaryKey = new HashMap<>();		
		Map<Long, Question> questionsWithPrimaryKey = new HashMap<>();
		
		while(rs.next()) {
			//TODO get pk (isbn) from this row in result set
			
			//TODO if no entry in map for this, then add it
			
			//TODO get question pk
			
			//TODO if question pk does not already exist in map, then extract and add it
			
			//TODO no matter what, always extract incorrect answer specific here
			
		}
		
		log.debug("At the end of ListOfAssessmentResultSetExtractorImpl.extractData returning {} number of assessments", assessments.size());
		return assessments;
	}
}