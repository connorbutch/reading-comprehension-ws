package com.connor.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

import org.slf4j.Logger;

import com.connor.model.IncorrectAnswer;
import com.connor.model.Question;

/**
 * This class provides a common base for result set extractors.  It contains shared code for extracting a question, and an incorrect answer from
 * a result set for reuse across other result set extractors.
 * @author connor
 *
 */
public abstract class AbstractBaseResultSetExtractor implements DaoConstants {

	/**
	 * 
	 */
	protected final Logger log;

	/**
	 * 
	 * @param log
	 */
	protected AbstractBaseResultSetExtractor(Logger log) {
		this.log = log;
	}

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	protected Optional<Question> getAndPopulateQuestion(ResultSet rs) throws SQLException { 
		Question question = null;
		final Question q = new Question();
		while(rs.next()) {
			//since auto increments starts with 1, zero means left outer join returned null for this
			if(rs.getLong(QUESTION_ID_STR) != 0l){
				question = new Question();
				question.setQuestionId(rs.getLong(QUESTION_ID_STR));
				log.debug("got question id from result set");
				question.setAssessmentIsbn(rs.getLong(ASSESSMENT_ISBN));
				log.debug("Got assessment isbn (for use as fk in question) from result set");
				question.setQuestionText(trimIfNotNull(rs.getString(QUESTION_TEXT)));
				log.debug("Got question text from result set");
				question.setCorrectAnswer(trimIfNotNull(rs.getString(CORRECT_ANSWER)));
				log.debug("Got correct answer from result set");
				getAndPopulateIncorrectAnswer(rs).ifPresent(incorrectAnswer -> {
					if(q.getIncorrectAnswers() == null) {
						log.debug("Instantiating the incorrect answer array in the response from db");
						q.setIncorrectAnswers(new ArrayList<>());
					}
					q.getIncorrectAnswers().add(incorrectAnswer);
					log.debug("Successfully added incorrect answer  with id {} to question", incorrectAnswer.getAnswerId());
				});
			}		

		}

		question.setIncorrectAnswers(q.getIncorrectAnswers());		
		return Optional.ofNullable(question);
	}

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	protected Optional<IncorrectAnswer> getAndPopulateIncorrectAnswer(ResultSet rs) throws SQLException { 
		IncorrectAnswer answer = null;

		//since auto increments starts with 1, zero means left outer join returned null for this
		if(rs.getLong(INCORRECT_ANSWER_SID) != 0) {
			answer = new IncorrectAnswer();
			answer.setAnswerId(rs.getInt(INCORRECT_ANSWER_SID));
			log.debug("Got incorrect answer sid from result set");
			answer.setQuestionId(rs.getInt(ANSWER_TABLE_QUESTION_SID));
			log.debug("Got question id (for use as fk in incorrect answer) from result set");
			answer.setAnswerText(rs.getString(INCORRECT_ANSWER_TEXT));
			log.debug("Got incorrect answer text from result set");
		}		

		return Optional.ofNullable(answer);
	}	

	/**
	 * If string is not null, trims and returns it.  Else, return the original string (null).  This is needed because
	 * often databases will automatically pad until the end of a string (especially when char is used instead of varchar).
	 * 
	 * Example, field is declared VARCHAR(16) and value stored is "string".  It will sometimes come back as "string         "
	 * @param stringToTrim
	 * @return
	 */
	protected String trimIfNotNull(String stringToTrim) {
		return (stringToTrim == null)? null: stringToTrim.trim();
	}
}