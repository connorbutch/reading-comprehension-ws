package com.connor.dao;

import com.connor.exception.ReadingSystemException;
import com.connor.model.Assessment;
import com.connor.model.Question;

import java.util.List;
import java.util.Optional;

/**
 * Holds the dao layer for interacting with db.
 * @author connor
 *
 */
public interface AssessmentDao {

	/**
	 * Returns an optional associated with this assessment.
	 * @param isbn
	 * @throws ReadingSystemException when something goes wrong
	 * @return
	 */
	Optional<Assessment> getAssessmentForIsbn(long isbn);	
	
	/**
	 * Adds an assessment.  Will return true if could add, false if one already exists for isbn (pk)
	 * @param assessment
     * @throws ReadingSystemException when something goes wrong
	 */
	boolean addAssessment(Assessment assessment);
	
	/**
	 * Verifies an assessment
	 * @param isbn
	 * @param isVerified
	 * @return true if an assessment existed with isbn, else false
     * @throws ReadingSystemException when something goes wrong
	 */
	boolean verifyAssessment(long isbn, boolean isVerified);
	
	/**
	 * Adds a question, and returns the generated key for use in hateoas uri.
	 * @param question
	 * @throws ReadingSystemException when something goes wrong
	 * @return the generated key from the database
	 */
	long addQuestion(Question question);
	
	/**
	 * Used to check if an assessment with a given isbn exists
	 * @param isbn
	 * @throws ReadingSystemException when something goes wrong
	 * @return true if assessment with that isbn exists
	 */
	boolean doesAssessmentExistWithIsbn(long isbn);
	
	/**
	 * 
	 * @param questionId
	 * @return an optional, which is present if question exists for the specified id
	 */
	Optional<Question> getQuestionForId(long questionId);	
	
	/**
	 * Used to get assessments matching criteria.
	 * @param limit
	 * @param offset
     * @throws ReadingSystemException when something goes wrong
	 * @return if nothing matching criteria, then an empty list; otherwise, a populated list.
	 */
	List<Assessment> getAssessments(int limit, int offset);
	
	/**
	 * 
	 * @param limit
	 * @param offset
	 * @param createdTeacherId
	 * @return
	 */
	List<Assessment> getAssessmentsCreatedByTeacher(int limit, int offset, int createdTeacherId);
	
	/**
	 * Used to get the total number of matching assessments.  
     * @throws ReadingSystemException when something goes wrong
	 * @return total number of matching assessments
	 */
	long getTotalNumberOfAssessments();
	
	/**
	 * 
	 * @param createdTeacherId
	 * @return
	 */
	int getTotalNumberOfAssessmentsCreatedByTeacher(int createdTeacherId);
}