package com.connor.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Holds constants related to dao to keep our dao implementation cleaner.
 * @author connor
 *
 */
public interface DaoConstants {

	/**
	 * 
	 */
	public static final String NOT_FOUND_FORMAT_STR = "Sql not found for property %s";
	
	/**
	 * 
	 */
	public static final String BLANK_FORMAT_STR = "Sql was blank for property %s.  This will likely cause errors when running sql";
	
	
	//---------------------------------------------------------------------------------------------------------------------------------
	//below this line is property names for retrieving the sql
	
	/**
	 * 
	 */
	public static final String GET_ASSESSMENT_BY_ISBN_KEY = "getAssessmentByIsbn";
	
	/**
	 * 
	 */
	public static final String ADD_ASSESSMENT_KEY = "addAssessment";
	
	/**
	 * 
	 */
	public static final String VERIFY_ASSESSMENT_KEY = "verifyAssessment";
	
	/**
	 * 
	 */
	public static final String ADD_QUESTION_KEY = "addQuestion";
	
	/**
	 * 
	 */
	public static final String DOES_ASSESSMENT_EXIST_KEY = "doesAssessmentExist";
	
	/**
	 * 
	 */
	public static final String ADD_INCORRECT_ANSWER_KEY = "addIncorrectAnswer";
	
	/**
	 * 
	 */
	public static final String GET_QUESTION_BY_ID_KEY = "getQuestion";
	
	/**
	 * Use this for getting all assessments without a teacher id.
	 */
	public static final String GET_ALL_ASSESSMENTS = "getAllAssessments";
	
	/**
	 * Use this to get all assessments for a given teacher.
	 */
	public static final String GET_ALL_ASSESSMENTS_BY_TEACHER = "getAssessmentsForTeacher";
	
	/**
	 * Use this to get the total number of assessments
	 */
	public static final String GET_TOTAL_NUMBER_OF_ASSESSMENTS = "getTotalNumberOfAssessments";
	
	/**
	 * Use this to get total number of assessments by teacher
	 */
	public static final String GET_NUMBER_OF_ASSESSMENTS_FOR_TEACHER = "getNumberOfAssessmentsForTeacher";
	
	
	//---------------------------------------------------------------------------------------------------------------------------------
		//these come from assessment table
		/**
		 * 
		 */
		public static final String ISBN_STR = "ISBN";

		/**
		 * 
		 */
		public static final String AUTHOR_FIRST_NAME = "AUTHOR_FIRST_NAME";

		/**
		 * 
		 */
		public static final String AUTHOR_LAST_NAME = "AUTHOR_LAST_NAME";

		/**
		 * 
		 */
		public static final String TITLE = "TITLE";

		/**
		 * 
		 */
		public static final String NUMBER_OF_POINTS = "NUMBER_OF_POINTS";

		/**
		 * 
		 */
		public static final String IS_VERIFIED = "IS_VERIFIED";
		
		/**
		 * 
		 */
		public static final String READING_LEVEL = "READING_LEVEL";

		/**
		 * 
		 */
		public String CREATED_TEACHER_ID = "CREATED_TEACHER_ID";

		//-----------------------------------------------------------
		//these come from question table
		/**
		 * 
		 */
		public static final String QUESTION_ID_STR = "QUESTION_SID";

		/**
		 * 
		 */
		public static final String ASSESSMENT_ISBN = "ASSESSMENT_ISBN";

		/**
		 * 
		 */
		public static final String QUESTION_TEXT = "QUESTION_TEXT";

		/**
		 * 
		 */
		public static final  String CORRECT_ANSWER = "CORRECT_ANSWER";

		//-----------------------------------------------------------

		//these come from the incorrect answer table
		/**
		 * 
		 */
		public static final String INCORRECT_ANSWER_SID = "INCORRECT_ANSWER_SID";

		/**
		 * 
		 */
		public static final String ANSWER_TABLE_QUESTION_SID = "QUESTION_SID"; //note this happens to be same name in in incorrect table as question

		/**
		 * 
		 */
		public static final String INCORRECT_ANSWER_TEXT = "TEXT";
		
		
		
		//-----------------------------------------------------------
		//these come from general sql
		/**
		 * Limit parameter
		 */
		public static final String LIMIT = "LIMIT";
		
		/**
		 * Offset parameter
		 */
		public static final String OFFSET = "OFFSET";
		
		
		
		/**
		 * 
		 */
		public static final String ERROR_FORMAT_STR = "There was an error executing sql to %s.  The sql was %s and the args passed were %s";

	
	
}
