package com.connor.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.connor.model.Assessment;
import com.connor.model.IncorrectAnswer;
import com.connor.model.Question;

/**
 * 
 * @author connor
 *
 */
@Component
public class QuestionResultSetExtractorImpl extends AbstractBaseResultSetExtractor implements ResultSetExtractor<Question> {
	
	/**
	 * 
	 * @param log
	 */
	@Autowired
	public QuestionResultSetExtractorImpl(Logger log) {
		super(log);
	}
	
	@Override
	public Question extractData(ResultSet rs) throws SQLException, DataAccessException {
		return super.getAndPopulateQuestion(rs).get();
	}
}