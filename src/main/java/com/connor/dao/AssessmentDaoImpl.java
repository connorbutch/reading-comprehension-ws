package com.connor.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.connor.exception.ReadingSystemException;
import com.connor.model.Assessment;
import com.connor.model.IncorrectAnswer;
import com.connor.model.Question;
import com.connor.util.SqlProvider;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

/**
 * 
 * @author connor
 *
 */
@Repository
public class AssessmentDaoImpl implements AssessmentDao, DaoConstants {	

	/**
	 * Logger instance
	 */
	private final Logger log;

	/**
	 * Used to get sql from properties file (to externalize from our source code per best practices)
	 */
	private final SqlProvider sqlProvider;

	/**
	 * For interacting with db; I prefer named template over regular jdbc template, as you don't have to worry about order of parameters
	 */
	private final NamedParameterJdbcTemplate namedTemplate;

	/**
	 * ONLY EVER USE THIS FOR QUERIES WITH NO PARAMETERS.
	 */
	private final JdbcTemplate template;

	/**
	 * Used to extract an assessment from a result set, joining with question and incorrect answer tables
	 */
	private final ResultSetExtractor<Assessment> assessmentExtractor;

	/**
	 * Used to extract a question from a result set, joining with incorrect answer table
	 */
	private final ResultSetExtractor<Question> questionExtractor;	

	/**
	 * 
	 */
	private final ResultSetExtractor<List<Assessment>> listOfAssessmentExtractor;


	/**
	 * Cdi-enabled constructor.
	 * @param log
	 * @param sqlProvider
	 * @param namedTemplate
	 * @param template
	 * @param assessmentExtractor
	 * @param questionExtractor
	 * @param listOfAssessmentExtractor
	 */
	@Autowired
	public AssessmentDaoImpl(Logger log, SqlProvider sqlProvider, NamedParameterJdbcTemplate namedTemplate,
			JdbcTemplate template, ResultSetExtractor<Assessment> assessmentExtractor,
			ResultSetExtractor<Question> questionExtractor,
			ResultSetExtractor<List<Assessment>> listOfAssessmentExtractor) { 
		this.log = log;
		this.sqlProvider = sqlProvider;
		this.namedTemplate = namedTemplate;
		this.template = template;
		this.assessmentExtractor = assessmentExtractor;
		this.questionExtractor = questionExtractor;
		this.listOfAssessmentExtractor = listOfAssessmentExtractor;
	}

	@Override
	public Optional<Assessment> getAssessmentForIsbn(long isbn) {
		log.debug("In assessmentDaoImpl.getAssessmentForIsbn for value {}", isbn);

		Assessment assessment = null;

		String propertyKey = GET_ASSESSMENT_BY_ISBN_KEY;
		log.debug("Reading property {} from provider", propertyKey);

		String sql = sqlProvider.getSql(propertyKey)
				.orElseThrow(() -> {
					return new ReadingSystemException(propertyNotFoundMessage(propertyKey)); 
				});

		if(StringUtils.isBlank(sql)) {
			log.warn(warnSqlNullMessage(propertyKey));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(ISBN_STR, isbn); 

		log.debug("Attempting to get assessment by running sql {} with parameters {}", sql, parameters);


		try {
			assessment = namedTemplate.query(sql, parameters, assessmentExtractor);
		}catch(DataAccessException e) {
			String errorMessage = getQueryErrorMessage("querying for assessment based on isbn", sql, parameters);
			throw new ReadingSystemException(errorMessage, e); 
		}

		log.debug("Returning optional of {} for search on isbn {}", assessment, isbn);
		return Optional.ofNullable(assessment); 
	}

	@Override
	public boolean addAssessment(Assessment assessment) {
		log.debug("In AssessmentDaoImpl.addAssessment for assessment {}", assessment);	

		boolean wasAddedWithoutDuplicateKey = true;

		String propertyKey = ADD_ASSESSMENT_KEY;
		log.debug("Reading property {} from provider", propertyKey);
		String sql = sqlProvider.getSql(propertyKey)
				.orElseThrow(() -> {
					return new ReadingSystemException(propertyNotFoundMessage(propertyKey)); 
				});

		if(StringUtils.isBlank(sql)) {
			log.warn(warnSqlNullMessage(propertyKey));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(ISBN_STR, assessment.getIsbn())
				.addValue(AUTHOR_FIRST_NAME, assessment.getAuthorFirstName())
				.addValue(AUTHOR_LAST_NAME, assessment.getAuthorLastName())
				.addValue(TITLE, assessment.getTitle())
				.addValue(NUMBER_OF_POINTS, assessment.getPoints())
				.addValue(IS_VERIFIED, assessment.isVerified())
				.addValue(READING_LEVEL, assessment.getReadingLevel())
				.addValue(CREATED_TEACHER_ID, assessment.getCreatedTeacherId());
		int numberOfRowsAffected = -1;

		try {
			numberOfRowsAffected = namedTemplate.update(sql, parameters);
		}catch(DuplicateKeyException e) {
			//in this case, set to false, so we know to handle as 404 while keeping our service independent of dao structure/technology
			wasAddedWithoutDuplicateKey = false;
			log.warn("Experienced a duplicate key exception when attempting to add an assessment; this should likely be returned to client as 404");
		}
		catch(DataAccessException e) {			
			String errorMessage = getQueryErrorMessage("Adding an assessment", sql, parameters);
			throw new ReadingSystemException(errorMessage, e); 
		}

		if(wasAddedWithoutDuplicateKey) {
			log.debug("Successfully added assessment {}.  The database updated/added {} rows", assessment, numberOfRowsAffected); //TODO maybe add sql parameter source rather than assessment
		}

		return wasAddedWithoutDuplicateKey;
	}

	@Override
	public boolean verifyAssessment(long isbn, boolean isVerified) {
		log.debug("In AssessmentDaoImpl.verifyAssessment for isbn {} and verified {}", isbn, isVerified);

		String propertyKey = VERIFY_ASSESSMENT_KEY;
		log.debug("Reading property {} from provider", propertyKey);
		String sql = sqlProvider.getSql(propertyKey)
				.orElseThrow(() -> {
					return new ReadingSystemException(propertyNotFoundMessage(propertyKey)); 
				});

		if(StringUtils.isBlank(sql)) {
			log.warn(warnSqlNullMessage(propertyKey));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(ISBN_STR, isbn)
				.addValue(IS_VERIFIED, isVerified);

		int numberOfRowsAffected = -1;

		try {
			numberOfRowsAffected = namedTemplate.update(sql, parameters);
		}catch(DataAccessException e) {
			String errorMessage = getQueryErrorMessage("Verifying an assessment", sql, parameters);
			throw new ReadingSystemException(errorMessage, e); 
		}

		boolean didUpdate = numberOfRowsAffected > 0;
		String didUpdateStr = (didUpdate)? "DID": "DIDN'T";
		log.debug("When setting verification value assessment for isbn {} to {}, we {} update at least one row", isbn, isVerified, didUpdateStr);
		return didUpdate;
	}

	@Override
	public long addQuestion(Question question) {		
		log.debug("In AssessmentDaoImpl.addQuestion for question {}", question);

		String propertyKey = ADD_QUESTION_KEY;
		log.debug("Reading property {} from provider", propertyKey);
		String sql = sqlProvider.getSql(propertyKey)
				.orElseThrow(() -> {
					return new ReadingSystemException(propertyNotFoundMessage(propertyKey)); 
				});

		if(StringUtils.isBlank(sql)) {
			log.warn(warnSqlNullMessage(propertyKey));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(ASSESSMENT_ISBN, question.getAssessmentIsbn())
				.addValue(QUESTION_TEXT, question.getQuestionText())
				.addValue(CORRECT_ANSWER, question.getCorrectAnswer());
		KeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		int numberOfRowsAffected = -1;

		try {
			numberOfRowsAffected = namedTemplate.update(sql, parameters, generatedKeyHolder);
		}catch(DataAccessException e) {
			//in the current design, there is a slight chance (in situations of high number of requests, say billions) that when this happens, it will return a 500 instead of 404, but to avoid, we would either need to write dao logic in service, or move logic to dao, so we will live with this 
			if(e instanceof DataIntegrityViolationException) {
				log.warn("We recieved a DataIntegrityViolationException, this means the assessment does not exist; this should return a 404, but will lead to 500 being returned");
			}
			String errorMessage = getQueryErrorMessage("Adding a question", sql, parameters);
			throw new ReadingSystemException(errorMessage, e); 			
		}

		long createdQuestionId = generatedKeyHolder.getKey().longValue();
		log.debug("Successfully added to question table for isbn {}, we created question with id {} and updated {} number of rows.  We will now add to incorrect answer table", question.getAssessmentIsbn(), generatedKeyHolder.getKey(), numberOfRowsAffected);


		question.getIncorrectAnswers().forEach((incorrectAnswer) -> {		
			long incorrectAnswerId = addIncorrectAnswer(createdQuestionId, incorrectAnswer);
			log.debug("Successfully added entry to incorrect answer table for question with id {} incorrect answer had generated id {}", question.getQuestionId(), incorrectAnswerId);
		});

		log.debug("At the end of AssessmentDaoImpl.addQuestion successfully added question and all incorrect answers.  We will return question id {}", createdQuestionId);
		return createdQuestionId;
	}	

	@Override
	public boolean doesAssessmentExistWithIsbn(long isbn) {
		boolean doesAssessmentExist = false;

		log.debug("In AssessmentDaoImpl.doesAssessmentExistWithIsbn for isbn {}", isbn);

		String propertyKey = DOES_ASSESSMENT_EXIST_KEY;

		log.debug("Reading property {} from provider", propertyKey);
		String sql = sqlProvider.getSql(propertyKey)
				.orElseThrow(() -> {
					return new ReadingSystemException(propertyNotFoundMessage(propertyKey)); 
				});

		if(StringUtils.isBlank(sql)) {
			log.warn(warnSqlNullMessage(propertyKey));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(ISBN_STR, isbn);
		int countOfMatchingRows = -1;

		try {
			countOfMatchingRows = namedTemplate.queryForObject(sql, parameters, Integer.class);
		}catch(DataAccessException e) {
			String errorMessage = getQueryErrorMessage("Checking if an assessment exists", sql, parameters);
			throw new ReadingSystemException(errorMessage, e); 
		}

		doesAssessmentExist = countOfMatchingRows == 1;		
		log.debug("Assessment exists with isbn {} evalauated to {} (true indicates assessment exists for isbn)", isbn, doesAssessmentExist);
		return doesAssessmentExist;
	}	

	@Override
	public Optional<Question> getQuestionForId(long questionId) {
		log.debug("In AssessmentDaoImpl.getQuestionForId for id {}", questionId);
		Question question = null;;
		String propertyKey = GET_QUESTION_BY_ID_KEY;
		log.debug("Reading property {} from provider", propertyKey);

		String sql = sqlProvider.getSql(propertyKey)
				.orElseThrow(() -> {
					return new ReadingSystemException(propertyNotFoundMessage(propertyKey)); 
				});

		if(StringUtils.isBlank(sql)) {
			log.warn(warnSqlNullMessage(propertyKey));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(QUESTION_ID_STR, questionId); 

		log.debug("Attempting to get question by running sql {} with parameters {}", sql, parameters);

		try {
			question = namedTemplate.query(sql, parameters, questionExtractor);
		}catch(DataAccessException e) {
			String errorMessage = getQueryErrorMessage("querying for question on id", sql, parameters);
			throw new ReadingSystemException(errorMessage, e); 
		}

		log.debug("At the end of AssessmentDaoImpl.getQuestionForId for id {}.  We will return {} as question value (in optional)", questionId, question);
		return Optional.ofNullable(question);
	}			

	@Override
	public long getTotalNumberOfAssessments() {
		log.debug("In AssessmentDaoImpl.getTotalNumberOfAssessments");

		long numberOfAssessments;

		String propertyKey = GET_ALL_ASSESSMENTS;
		log.debug("Reading property {} from provider", propertyKey);

		String sql = sqlProvider.getSql(propertyKey)
				.orElseThrow(() -> {
					return new ReadingSystemException(propertyNotFoundMessage(propertyKey)); 
				});		

		if(StringUtils.isBlank(sql)) {
			log.warn(warnSqlNullMessage(propertyKey));
		}

		Class<Long> clazz = Long.class;

		try {
			numberOfAssessments = template.queryForObject(sql, clazz);
		}catch(DataAccessException e) {
			String errorMessage = getQueryErrorMessage("querying for all assessments", sql, clazz);
			throw new ReadingSystemException(errorMessage, e); 
		}

		log.debug("At the end of AssessmentDaoImpl.getTotalNumberOfAssessments, returning {}", numberOfAssessments);
		return numberOfAssessments;		
	}

	@Override
	public List<Assessment> getAssessmentsCreatedByTeacher(int limit, int offset, int createdTeacherId) {
		log.debug("In AssessmentDaoImpl.getAssessmentsWithTeacherId for limit {}, offset {}, and created teacher id {}", limit, offset, createdTeacherId);

		List<Assessment> assessments;

		String propertyKey = GET_ALL_ASSESSMENTS_BY_TEACHER;
		log.debug("Reading property {} from provider", propertyKey);

		String sql = sqlProvider.getSql(propertyKey)
				.orElseThrow(() -> {
					return new ReadingSystemException(propertyNotFoundMessage(propertyKey)); 
				});

		if(StringUtils.isBlank(sql)) {
			log.warn(warnSqlNullMessage(propertyKey));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(LIMIT, limit)
				.addValue(OFFSET, offset)
				.addValue(CREATED_TEACHER_ID, createdTeacherId);

		try {
			assessments = namedTemplate.query(sql, parameters, listOfAssessmentExtractor);
		}catch(DataAccessException e) {
			String errorMessage = getQueryErrorMessage("getting all assessments for teacher", sql, parameters);
			throw new ReadingSystemException(errorMessage, e); 
		}

		log.debug("At the end of AssessmentDaoImpl.getAssessmentsWithTeacherId for limit {}, offset {}, and created teacher id {}.  Returning list with size {}", limit, offset, createdTeacherId, assessments.size());
		return assessments;
	}

	@Override
	public int getTotalNumberOfAssessmentsCreatedByTeacher(int createdTeacherId) {
		log.debug("In AssessmentDaoImpl.getNumberOfAssessmentsByTeacher for teacher id {}", createdTeacherId);

		int numberOfAssessments;

		String propertyKey = GET_ALL_ASSESSMENTS_BY_TEACHER;
		log.debug("Reading property {} from provider", propertyKey);

		String sql = sqlProvider.getSql(propertyKey)
				.orElseThrow(() -> {
					return new ReadingSystemException(propertyNotFoundMessage(propertyKey)); 
				});		

		if(StringUtils.isBlank(sql)) {
			log.warn(warnSqlNullMessage(propertyKey));
		}

		Class<Integer> clazz = Integer.class;

		try {
			numberOfAssessments = template.queryForObject(sql, clazz);
		}catch(DataAccessException e) {
			String errorMessage = getQueryErrorMessage("querying for all assessments by a given teacher", sql, clazz);
			throw new ReadingSystemException(errorMessage, e); 
		}

		log.debug("At the end of AssessmentDaoImpl.getNumberOfAssessmentsByTeacher for teacher id {}; returning {}", createdTeacherId, numberOfAssessments);
		return numberOfAssessments;
	}	

	@Override
	public List<Assessment> getAssessments(int limit, int offset){
		log.debug("In AssessmentDaoImpl.getAssessments for limit {} and offset {}", limit, offset);

		List<Assessment> assessments; 

		String propertyKey = GET_ALL_ASSESSMENTS;
		log.debug("Reading property {} from provider", propertyKey);

		String sql = sqlProvider.getSql(propertyKey)
				.orElseThrow(() -> {
					return new ReadingSystemException(propertyNotFoundMessage(propertyKey)); 
				});

		if(StringUtils.isBlank(sql)) {
			log.warn(warnSqlNullMessage(propertyKey));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(LIMIT, limit)
				.addValue(OFFSET, offset);

		try {
			assessments = namedTemplate.query(sql, parameters, listOfAssessmentExtractor);
		}catch(DataAccessException e) {
			String errorMessage = getQueryErrorMessage("getting all assessments (no teacher id)", sql, parameters);
			throw new ReadingSystemException(errorMessage, e); 
		}

		log.debug("At the end of AssessmentDaoImpl.getAssessments for limit {} and offset {}.  Returning list of size {}", limit, offset, assessments.size());
		return assessments; 
	}

	/**
	 * 
	 * @param questionId
	 * @param incorrectAnswer
	 * @return
	 */
	protected long addIncorrectAnswer(long questionId, IncorrectAnswer incorrectAnswer) {
		log.debug("In AssessmentDaoImpl.addIncorrectAnswer for questionId {} and incorrect answer {}", questionId, incorrectAnswer);
		long incorrectAnswerId = -1;		
		String propertyKey = ADD_INCORRECT_ANSWER_KEY;
		log.debug("Reading property {} from provider", propertyKey);
		String sql = sqlProvider.getSql(propertyKey).orElseThrow(() -> {
			return new ReadingSystemException(propertyNotFoundMessage(propertyKey)); 
		});

		if(StringUtils.isBlank(sql)) {
			log.warn(warnSqlNullMessage(propertyKey));
		}

		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue(QUESTION_ID_STR, questionId)
				.addValue(INCORRECT_ANSWER_TEXT, incorrectAnswer.getAnswerText());
		KeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		int numberOfRowsAffected = -1;

		try {
			numberOfRowsAffected = namedTemplate.update(sql, parameters, generatedKeyHolder);
		}catch(DataAccessException e) {
			String errorMessage = getQueryErrorMessage("Adding an incorrect answer", sql, parameters);
			throw new ReadingSystemException(errorMessage, e); 
		}

		//there is an error with mysql driver (well-documented) that randomly occurs making it sometimes select as bigint, so the following lines avoid that error
		if(generatedKeyHolder.getKey() instanceof java.math.BigInteger) {
			incorrectAnswerId = generatedKeyHolder.getKey().longValue();
		}else {
			incorrectAnswerId = (long)generatedKeyHolder.getKey();
		}

		log.debug("At end of adding incorrect answer, and got generated key {}.  The number of rows affected was {}", incorrectAnswerId, numberOfRowsAffected);
		return incorrectAnswerId;
	}

	/**
	 * Used to have consistent property not found message.
	 * @param property
	 * @return
	 */
	protected String propertyNotFoundMessage(String property) {
		return String.format(NOT_FOUND_FORMAT_STR, property);
	}

	/**
	 * 
	 * @param property
	 * @return
	 */
	protected String warnSqlNullMessage(String property) {
		return String.format(BLANK_FORMAT_STR, property);
	}

	/**
	 * 
	 * @param functionality
	 * @param sql
	 * @param parameters
	 * @return
	 */
	protected String getQueryErrorMessage(String functionality, String sql, SqlParameterSource parameters) { //NOTE: could extract this out if we ever have more daos . . . 
		StringBuilder parametersSb = new StringBuilder();

		//NOTE: intentional choice not to use streams here, as we pluck more than just list here, also, more memory efficient
		for(int i = 0; i < parameters.getParameterNames().length; ++i) {
			String parameter = parameters.getParameterNames()[i];
			parametersSb.append("parameter: ");
			parametersSb.append(parameter);
			parametersSb.append(" value: ");
			parametersSb.append(parameters.getValue(parameter));
			parametersSb.append(" sql type: ");
			parametersSb.append(parameters.getSqlType(parameter));
			parametersSb.append(" type name: ");
			parametersSb.append(parameters.getTypeName(parameter));
			if(i != parameters.getParameterNames().length - 1) {
				parametersSb.append(".  ");
			}
		}		

		return String.format(ERROR_FORMAT_STR, functionality, sql, parametersSb.toString());
	}	

	/**
	 * 
	 * @param functionality
	 * @param sql
	 * @param type
	 * @return
	 */
	protected String getQueryErrorMessage(String functionality, String sql, Class<?> type) {
		return String.format("There was an error when %s, the sql ran was %s.  The class was %s", functionality, sql, type.toString()); //TODO maybe check for null ptr here
	}


	//--------------------------------------------------------------------------------------------------------------------------------------
	//functions beneath here are nice to have if run into sql with many parameters; can implement them if needed
	/**
	 * At times, it can be really annoying to find if there are extra, or more often misspelled parameters.  This function returns extra
	 * parameters found in the parameter source, but not in sql.
	 * @param sql
	 * @param parameters
	 * @return
	 */
	protected List<String> findExtraParametersInParameterSourceNotInArgs(String sql, SqlParameterSource parameters) {		
		Set<String> stringParameters = getParametersFromSqlString(sql);
		return Arrays.stream(parameters.getParameterNames())
				.filter(parameter ->  { 
					return !stringParameters.contains(parameter); 
				})
				.collect(Collectors.toList());		
	}

	/**
	 * This returns expected parameters from the sql, not found in parameter source supplying the args.
	 * @param sql
	 * @param parameters
	 * @return
	 */
	protected List<String> findExpectedParametersNotPresent(String sql, SqlParameterSource parameters){
		Set<String> stringParameters = getParametersFromSqlString(sql);
		Arrays.stream(parameters.getParameterNames()).forEach(parameter -> stringParameters.remove(parameter));
		return Arrays.asList((String[])stringParameters.toArray());
	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	protected Set<String> getParametersFromSqlString(String sql){
		return null;//TODO
	}
}