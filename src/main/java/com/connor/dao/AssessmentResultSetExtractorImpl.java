package com.connor.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.connor.model.Assessment;
import com.connor.model.IncorrectAnswer;
import com.connor.model.Question;

/**
 * Result set extractor for an assessment
 * @author connor
 *
 */
@Component
public class AssessmentResultSetExtractorImpl extends AbstractBaseResultSetExtractor implements ResultSetExtractor<Assessment> {

	/**
	 * 
	 * @param log
	 */
	@Autowired
	public AssessmentResultSetExtractorImpl(Logger log) {
		super(log);
	}

	@Override
	public Assessment extractData(ResultSet rs) throws SQLException, DataAccessException {
		Assessment assessment = null;		
		Map<Long, Question> questionsToDbKey = new HashMap<>();

		//use this to keep track of if it is the first iteration, in which we should gather assessment specific info
		boolean isFirstIteration = true;

		//loop over the rows in the result set
		while(rs.next()) {
			//for the first iteration, populate the assessment, otherwise, save work by skipping this step
			if(isFirstIteration) {
				assessment = getAndPopulateAssessment(rs);
				log.debug("Populated assessment from result set {}", assessment);
				isFirstIteration = false;
			}

			//get the question id and see if it is in the map yet
			long questionId = rs.getLong(QUESTION_ID_STR);
			log.debug("Successfully got question id {}", questionId);
			Question questionForKey = questionsToDbKey.get(questionId);
		}

		//only run if assessment is not null
		if(assessment != null) {
			//if there are no questions, then warn (will cause exception later)
			if(questionsToDbKey.isEmpty() ) {
				log.warn("There are no questions mapped to assessment with isbn {}.  This is likely cause an error.", assessment.getIsbn());
			}else {
				//loop over our list of questions, and add them to the assessment
				assessment.setQuestions(
						questionsToDbKey
						.values()
						.stream()
						.collect(Collectors.toList())				
						);
				log.debug("Set questions for assessment with isbn {} to values {}", assessment.getIsbn(), assessment.getQuestions());
			}
		}

		log.debug("At end of result set extractor, returning assessment {}", assessment);
		return assessment;
	}

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException 
	 */
	protected Assessment getAndPopulateAssessment(ResultSet rs) throws SQLException {
		Assessment assessment = new Assessment();		
		assessment.setIsbn(rs.getLong(ISBN_STR));
		log.debug("Got assessment isbn from result set");
		assessment.setAuthorFirstName(trimIfNotNull(rs.getString(AUTHOR_FIRST_NAME)));
		log.debug("Got author first name from result set");
		assessment.setAuthorLastName(trimIfNotNull(rs.getString(AUTHOR_LAST_NAME)));
		log.debug("Got author last name from result set");
		assessment.setTitle(trimIfNotNull(rs.getString(TITLE)));
		log.debug("Got assessment title from result set");
		assessment.setReadingLevel(rs.getDouble(READING_LEVEL));
		log.debug("Got reading level from result set");
		assessment.setVerified(rs.getBoolean(IS_VERIFIED));
		log.debug("Got is verified from result set");
		assessment.setCreatedTeacherId(rs.getInt(CREATED_TEACHER_ID));
		log.debug("Got created teacher id from result set");
		assessment.setNumberOfPoints(rs.getDouble(NUMBER_OF_POINTS));
		log.debug("Got number of points for an assessment from result set");
		assessment.setQuestions(new ArrayList<>()); //just so avoid null ptrs
		return assessment;
	}	
}