package com.connor.cdi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import com.connor.controller.AssessmentController;
import com.connor.conversion.AssessmentAssemblerImpl;
import com.connor.conversion.QuestionAssemblerImpl;
import com.connor.model.Assessment;
import com.connor.model.Question;
import com.connor.model.dto.AssessmentDTO;
import com.connor.model.dto.QuestionDTO;

/**
 * 
 * @author connor
 *
 */
@Configuration
public class AssemblerProducer {

	@Bean
	@Primary
	@Qualifier("assessment") //NOTE: this is needed because of type erasure at runtime
	public RepresentationModelAssemblerSupport<Assessment, AssessmentDTO> getAssessmentAssembler(InjectionPoint ip){
		Logger log = LoggerFactory.getLogger(AssessmentAssemblerImpl.class);
		return new AssessmentAssemblerImpl(AssessmentController.class, AssessmentDTO.class, log);
	}	
	
	@Bean
	@Primary
	@Qualifier("question") //NOTE: this is needed because of type erasure at runtime
	public RepresentationModelAssemblerSupport<Question, QuestionDTO> getQuestionAssembler(InjectionPoint ip) {
		Logger log = LoggerFactory.getLogger(QuestionAssemblerImpl.class);
		return new QuestionAssemblerImpl(AssessmentController.class, QuestionDTO.class, log);
	}
}