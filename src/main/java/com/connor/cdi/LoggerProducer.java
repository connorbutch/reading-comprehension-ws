package com.connor.cdi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * @author connor
 *
 */
@Configuration //let this be picked up for cdi beans
public class LoggerProducer {

	/**
	 * Producer meethod for logger.
	 * @param ip
	 * @return
	 */
	@Bean
	public Logger getLogger(InjectionPoint ip) {
		return LoggerFactory.getLogger(getClass(ip));
	}
	
	/**
	 * 
	 * @param ip
	 * @return
	 */
	protected Class<?> getClass(InjectionPoint ip){
		Class<?> clazz = LoggerProducer.class; //should never happen . . . helps make unit testing easier
		if(ip != null) {
			if(ip.getField() != null) {
				clazz = ip.getField().getType();
			}else if(ip.getMethodParameter() != null) {
				clazz = ip.getMethodParameter().getParameterType();
			}
		}		
		return clazz;
	}	
}