package com.connor.conversion;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import com.connor.controller.AssessmentController;
import com.connor.model.IncorrectAnswer;
import com.connor.model.Question;
import com.connor.model.dto.IncorrectAnswerDTO;
import com.connor.model.dto.QuestionDTO;

/**
 * 
 * @author connor
 *
 */
//DON'T ANNOTATE THIS WITH COMPONENT
public class QuestionAssemblerImpl extends RepresentationModelAssemblerSupport<Question, QuestionDTO> {

	private final Logger log;
	
	/**
	 * 
	 * @param controllerClass
	 * @param resourceType
	 * @param log
	 */
	public QuestionAssemblerImpl(Class<?> controllerClass, Class<QuestionDTO> resourceType, Logger log) {
		super(controllerClass, resourceType);
		this.log = log;
	}

	@Override
	public QuestionDTO toModel(Question question) {
		log.debug("Assesembling question dto from question");
		QuestionDTO questionDTO = null;
		if(question != null) {
			questionDTO = new QuestionDTO();
			questionDTO.setCorrectAnswer(question.getCorrectAnswer());
			questionDTO.setIncorrectAnswers(getIncorrectAnswers(question.getIncorrectAnswers()));
			questionDTO.setQuestionText(question.getQuestionText());
			questionDTO.add(linkTo(methodOn(AssessmentController.class).getQuestion(question.getAssessmentIsbn(), question.getQuestionId())).withSelfRel());
		}
		
		log.debug("At the end of question assembler, question passed {} and question dto returned {}", question, questionDTO);
		return questionDTO;
	}
	
	/**
	 * 
	 * @param incorrectAnswers
	 * @return
	 */
	protected List<IncorrectAnswerDTO> getIncorrectAnswers(List<IncorrectAnswer> incorrectAnswers){
		List<IncorrectAnswerDTO> convertedAnswers = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(incorrectAnswers)) {
			convertedAnswers = incorrectAnswers
			.stream()
			.filter(incorrectAnswer -> { return incorrectAnswer != null; })
			.map(incorrectAnswer -> {
				IncorrectAnswerDTO dto = new IncorrectAnswerDTO();
				dto.setAnswerText(incorrectAnswer.getAnswerText());
				return dto;
			})
			.collect(Collectors.toList());			
		}		
		
		log.debug("Converted list of incorrect answers {} to incorrect answer dto {}", incorrectAnswers, convertedAnswers);;
		return convertedAnswers;
	}
}