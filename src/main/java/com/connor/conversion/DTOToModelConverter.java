package com.connor.conversion;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.connor.model.Assessment;
import com.connor.model.IncorrectAnswer;
import com.connor.model.Question;
import com.connor.model.dto.AssessmentDTO;
import com.connor.model.dto.IncorrectAnswerDTO;
import com.connor.model.dto.QuestionDTO;

/**
 * Class is responsible for converting from dto to associated model.
 * @author connor
 *
 */
@Component
public class DTOToModelConverter {
	
	/**
	 * 
	 */
	private final Logger log;
	
	/**
	 * 
	 * @param log
	 */
	@Autowired
	public DTOToModelConverter(Logger log) {
		this.log = log;
	}

	/**
	 * 
	 * @param isbn
	 * @param dto
	 * @return
	 */
	public Assessment convertAssessmentDTO(long isbn, AssessmentDTO dto) {
		log.debug("In DTOToModelConverter.convertAssessmentDTO for isbn {} and dto {}", isbn, dto);
		Assessment assessment = null;
		if(dto != null) {
			assessment = new Assessment();
			assessment.setAuthorFirstName(trimIfNeeded(dto.getAuthorFirstName()));
			assessment.setAuthorLastName(trimIfNeeded(dto.getAuthorLastName()));
			assessment.setCreatedTeacherId(dto.getCreatedTeacherId());
			assessment.setIsbn(isbn);
			assessment.setNumberOfPoints(dto.getPoints());
			if(CollectionUtils.isNotEmpty(dto.getQuestions())) {
				assessment.setQuestions(dto.getQuestions()
				.stream()
				.map(question -> {
					return convertQuestionDTO(isbn, question);
				})
				.collect(Collectors.toList())
				);
			}
			assessment.setReadingLevel(dto.getReadingLevel());
			assessment.setTitle(trimIfNeeded(dto.getTitle()));
			assessment.setVerified(false); //should always be false on first creation
		}		
		
		log.debug("For isbn {} and assessment dto {} returning converted assessment {}", isbn, dto, assessment);
		return assessment; 
	}
	
	/**
	 * 
	 * @param isbn
	 * @param dto
	 * @return
	 */
	public Question convertQuestionDTO(long isbn, QuestionDTO dto) {
		log.debug("In DTOToModelConverter.convertQuestionDTO for isbn {} and dto {}", isbn, dto);
		Question question = null;
		if(dto != null) {
			question = new Question();
			question.setAssessmentIsbn(isbn);
			question.setCorrectAnswer(dto.getCorrectAnswer());
			if(CollectionUtils.isNotEmpty(dto.getIncorrectAnswers())) {
				question.setIncorrectAnswers(dto.getIncorrectAnswers()
				.stream()
				.map(this::convertIncorrectAnswerDTO)
				.collect(Collectors.toList()));
			}
		}
		question.setQuestionText(trimIfNeeded(dto.getQuestionText()));
		log.debug("For isbn {} and dto {} returning converted value {}", isbn, dto, question);
		return question;
	}
	
	/**
	 * 
	 * @param dto
	 * @return
	 */
	public IncorrectAnswer convertIncorrectAnswerDTO(IncorrectAnswerDTO dto) {
		log.debug("In DTOToModelConverter.convertIncorrectAnswerDTO for dto {}", dto);
		IncorrectAnswer incorrectAnswer = null;
		if(dto != null) {
			incorrectAnswer = new IncorrectAnswer();
			incorrectAnswer.setAnswerText(dto.getAnswerText());
		}
		log.debug("For and dto {} returning value {}", dto, incorrectAnswer);
		return incorrectAnswer;
	}
	
	
	/**
	 * 
	 * @param stringToTrim
	 * @return
	 */
	protected String trimIfNeeded(String stringToTrim) { //TODO maybe look to pull this to util class, as we write similar methods sometimes
		return (stringToTrim == null)? null: stringToTrim.trim();
	}
}