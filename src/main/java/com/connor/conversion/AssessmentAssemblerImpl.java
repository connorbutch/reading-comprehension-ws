package com.connor.conversion;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import java.util.List;

import org.slf4j.Logger;

import com.connor.controller.AssessmentController;
import com.connor.model.Assessment;
import com.connor.model.dto.AssessmentDTO;
import com.connor.model.dto.IncorrectAnswerDTO;
import com.connor.model.dto.QuestionDTO;

/**
 * 
 * @author connor
 *
 */
//DON'T ANNOTATE THIS WITH COMPONENT
public class AssessmentAssemblerImpl extends RepresentationModelAssemblerSupport<Assessment, AssessmentDTO> {

	/**
	 * 
	 */
	private final Logger log;
	
	/**
	 * 
	 * @param controllerClass
	 * @param resourceType
	 * @param log
	 */
	public AssessmentAssemblerImpl(Class<?> controllerClass, Class<AssessmentDTO> resourceType, Logger log) {
		super(controllerClass, resourceType);		
		this.log = log;
	}

	@Override
	public AssessmentDTO toModel(Assessment assessment) {
		AssessmentDTO assessmentDTO = null;
		
		if(assessment != null) {
			log.debug("Assessment was NOT NULL, now to create assessmentDTO from it");
			assessmentDTO = new AssessmentDTO();
			assessmentDTO.setIsbn(assessment.getIsbn());
			assessmentDTO.setAuthorFirstName(assessment.getAuthorFirstName());
			assessmentDTO.setAuthorLastName(assessment.getAuthorLastName());
			assessmentDTO.setCreatedTeacherId(assessment.getCreatedTeacherId());
			assessmentDTO.setIsVerified(assessment.isVerified());
			assessmentDTO.setPoints(assessment.getNumberOfPoints());
			assessmentDTO.setReadingLevel(assessment.getReadingLevel());
			assessmentDTO.setTitle(assessment.getTitle());
			
			//TODO call get question and get incorrect answers
			
			//add our hateaos link
			assessmentDTO.add(linkTo(methodOn(AssessmentController.class).getAssessment(assessment.getIsbn())).withSelfRel());
		}	
		
		log.debug("Returning assessmentDTO {} from assembler", assessmentDTO);
		return assessmentDTO;	
	}
	
	/**
	 * 
	 * @param assessment
	 * @return
	 */
	protected List<QuestionDTO> getQuestionDTOs(Assessment assessment){
		return null; //TODO
	}
	
	/**
	 * 
	 * @param assessment
	 * @return
	 */
	protected List<IncorrectAnswerDTO> getIncorrectAnswerDTOs(Assessment assessment){
		return null; //TODO
	}
}