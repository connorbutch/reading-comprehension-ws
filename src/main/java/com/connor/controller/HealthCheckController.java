package com.connor.controller;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

/**
 * This handles basic health check requests.
 * @author connor
 *
 */
@Component
public class HealthCheckController implements HealthIndicator {

	private final Logger log;
	
	/**
	 * 
	 * @param log
	 */
	@Autowired //not required, but make explicit
	public HealthCheckController(Logger log) {
		this.log = log;
	}
	
	@Override
	public Health health() {
		log.debug("Health check request recieved");
		return Health.up().build();
	}
}