package com.connor.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.connor.model.Assessment;
import com.connor.model.Question;
import com.connor.model.dto.AssessmentDTO;
import com.connor.model.dto.QuestionDTO;
import com.connor.service.AssessmentService;

/**
 * Controller class for handling requests.
 * @author connor
 *
 */
@RestController()
public class AssessmentController {
	
	/**
	 * Format string for uri of assessments
	 */
	private static final String ASSESSMENT_LOCATION_HEADER_FORMAT_STR = "/assessments/%d";
	
	/**
	 * 
	 */
	private static final String QUESTION_LOCATION_HEADER_FORMAT_STR = "/assessments/%d/questions/%d";
	
	/**
	 * Used as header key to return total number of assessments.
	 */
	private static final String TOTAL_NUMBER_ASSESMENTS_HEADER_KEY = "totalNumberOfAssessments";
	
	/**
	 * Use to give client back limit used (in case they passed one that is too big)
	 */
	private static final String LIMIT_HEADER_KEY = "limit";
	
	
	
	/**
	 * Logger instance
	 */
	private final Logger log;
	
	/**
	 * 
	 */
	private final AssessmentService assessmentService;
	
	/**
	 * 
	 */
	private final RepresentationModelAssemblerSupport<Assessment, AssessmentDTO> assessmentAssembler;
	
	/**
	 * 
	 */
	private final RepresentationModelAssemblerSupport<Question, QuestionDTO> questionAssembler;
	
	/**
	 * Constructor.
	 * @param log
	 * @param assessmentService
	 * @param assessmentAssembler
	 * @param questionAssembler
	 */
	@Autowired
	public AssessmentController(Logger log, AssessmentService assessmentService, @Qualifier("assessment") 
 RepresentationModelAssemblerSupport<Assessment, AssessmentDTO> assessmentAssembler, @Qualifier("question") RepresentationModelAssemblerSupport<Question, QuestionDTO> questionAssembler) {
		this.log = log;
		this.assessmentService = assessmentService;
		this.assessmentAssembler = assessmentAssembler;
		this.questionAssembler = questionAssembler;
	}

	/**
	 * Handles get request for a single assessment
	 * @param isbn
	 * @return
	 */
	@GetMapping(path = "/assessments/{isbn}", produces = "application/json", name = "getAssessment")
	public AssessmentDTO getAssessment(@PathVariable(name="isbn")Long isbn){
		log.debug("Recieved a request to get an assessment by isbn {}", isbn);
		
		Assessment responseBody = assessmentService.getAssessmentForIsbn(isbn);
		log.debug("Successfully called service to get assessment.  Result is {}", responseBody);;
		
		AssessmentDTO assessmentDTO = assessmentAssembler.toModel(responseBody);
		log.debug("Successfully converted to dto using assembler.  Returning {} for isbn {} to client", assessmentDTO, isbn);
		
		return assessmentDTO;		
	}
	
	/**
	 * Handles a post request for a new assessment.
	 * @param isbn
	 * @param assessmentToAdd
	 * @return
	 */
	@PostMapping(path = "/assessments/{isbn}", produces = "application/json", name = "addAssessment") //NOTE: In this case, we return response entity, so we can use created with location header, which is not usable with just returning regular pojo
	public ResponseEntity<AssessmentDTO> addAssessment(@PathVariable(name="isbn") Long isbn, @RequestBody AssessmentDTO assessmentToAdd) {
		log.debug("Recieved a request to add an assessment for isbn {} request body {}", isbn, assessmentToAdd);
		
		assessmentService.addAssessment(isbn, assessmentToAdd);		
		log.debug("In AssessmentController.addAssessment and we successfully added the assessment.  Now we will use library to get it back");
		
		Assessment retrievedAssessment = assessmentService.getAssessmentForIsbn(isbn);
		log.debug("In AssessmentController.addAssessment, successfully retrieved assessment for isbn {}", isbn);
		
		AssessmentDTO assessmentDTO = assessmentAssembler.toModel(retrievedAssessment);
		log.debug("Successfully converted to dto using assembler.  Returning {} for isbn {} to client once its been added", assessmentDTO, isbn);
		
		URI location = getAssessmentURIForUseInLocationHeader(isbn);
		log.debug("In AssessmentController.addAssessment, returning {} as the value of location header", location.toString());
		
		return ResponseEntity
				.created(location)
				.body(assessmentDTO);
	}
	
	/**
	 * Handles a patch request for updating the verified value on an assessment.  The choice to use patch (as opposed to put)
	 * was very intentional; speak to Connor more if you have questions on this.
	 * @param isbn
	 * @param isVerified
	 * @return
	 */
	@PatchMapping(path = "/assessments/{isbn}", produces = "application/json", name = "verifyAssessment") 
	public AssessmentDTO verifyAssessment(@PathVariable(name="isbn") Long isbn, @RequestBody Boolean isVerified) {
		log.debug("Recieved a request to verify an assessment for isbn {} and verification value {}", isbn, isVerified);
		
		assessmentService.verifyAssessment(isbn, isVerified);
		log.debug("In AssessmentController.verifyAssessment and we successfully verified the assessment.  Now we will use library to get it back");

		Assessment retrievedAssessment = assessmentService.getAssessmentForIsbn(isbn);
		log.debug("In AssessmentController.verifyAssessment, successfully retrieved assessment for isbn {}", isbn);
		
		AssessmentDTO assessmentDTO = assessmentAssembler.toModel(retrievedAssessment);
		log.debug("Successfully converted assessment to dto using assembler.  Returning {} for isbn {} to client once its been added", assessmentDTO, isbn);
	
		return assessmentDTO;
	}
	
	/**
	 * Add a question for an assessment.
	 * @param isbn
	 * @param questionToAdd
	 * @return
	 */
	@PostMapping(path = "/assessments/{isbn}/questions", produces = "application/json", name = "addQuestion")
	public ResponseEntity<QuestionDTO> addQuestion(@PathVariable(name="isbn") Long isbn, @RequestBody QuestionDTO questionToAdd){
		log.debug("Recieved a request to add a question for isbn {} and question {}", isbn, questionToAdd);
		
		long questionId = assessmentService.addQuestion(isbn, questionToAdd);
		log.debug("In AssessmentController.addQuestion, and we successfully added the question with id {}.  Now we will get it back to return to client", questionId);
		
		Question createdQuestion = assessmentService.getQuestionForId(questionId);
		log.debug("After adding a question, we successfully retrieved it using the id for updated fields");
		
		QuestionDTO questionDTO = questionAssembler.toModel(createdQuestion);
		log.debug("Successfully converted question to dto using assembler.  Returning {} to client for isbn {} and question id {}", questionDTO, isbn, questionId);
		
		URI location = getQuestionURIForUseInLocationHeader(isbn, questionId);
		log.debug("In AssessmentController.addQuestion, returning {} as the value of location header", location.toString());

		return ResponseEntity
				.created(location)
				.body(questionDTO);		
	}
	
	@GetMapping(path = "/assessments/{isbn}/questions/{questionId}", produces = "application/json", name = "getQuestion")
	public QuestionDTO getQuestion(@PathVariable(name="isbn") Long isbn, @PathVariable(name="questionId") Long questionId) {
		throw new NotImplementedException("Just here for use in hateaos for now");
	}
	
	/**
	 * Used to query for assessments matching certain parameters.
	 * 
	 * NOTE: we use response entity here because we need to return total assessment count in the header.
	 * @param limit
	 * @param offset
	 * @param createdTeacherId
	 * @return
	 */
	@GetMapping(path = "/assessments", produces = "application/json", name = "queryForAssessments")
	public ResponseEntity<List<AssessmentDTO>> queryForAssessments(@RequestParam(name = "limit") Integer limit, @RequestParam(name = "offset") Integer offset, @RequestParam(name = "createdTeacherId") Integer createdTeacherId){
		log.debug("Recieved a request to query for assessments with limit {}, offset {}, and created teacher id {}", limit, offset, createdTeacherId);
		
		ImmutablePair<List<Assessment>, Integer> assessmentPair = assessmentService.getAssessments(limit, offset, createdTeacherId);
		List<Assessment> assessments = assessmentPair.getLeft();
		Integer limitUsed = assessmentPair.getRight();
		log.debug("After querying for assessments, we successfully retrieved list of size {}", assessments.size());
		
		List<AssessmentDTO> body = assessments
		.stream()
		.filter(assessment -> assessment != null)
		.map(assessmentAssembler::toModel)
		.collect(Collectors.toList());
		log.debug("Successfully converted all assessments to assessmentDTO");
		
		long totalNumberOfMatchingAsssesments = assessmentService.getTotalNumberOfMatchingAssessments(createdTeacherId);		
		log.debug("Successfully retrieved total number of matching assessments, which is {}", totalNumberOfMatchingAsssesments);
		
		//NOTE: we pass the actual limit used, which may be lower than the limit passed, back to the client
		return ResponseEntity
				.ok()
				.header(TOTAL_NUMBER_ASSESMENTS_HEADER_KEY, String.valueOf(totalNumberOfMatchingAsssesments))
				.header(LIMIT_HEADER_KEY, String.valueOf(limitUsed))
				.body(body);
	}
	
	//TODO add a get mapping here for question based on uri (so we can use in assembler), but make throw unsupported exception
	
	/**
	 * Get the URI for use in location header for assessments.  This is not nearly as important as we use HATEOAS, but do it to follow convention.
	 * @param isbn
	 * @return
	 */
	protected URI getAssessmentURIForUseInLocationHeader(Long isbn) {
		String uriStr = String.format(ASSESSMENT_LOCATION_HEADER_FORMAT_STR, isbn);
		return URI.create(uriStr);
	}
	
	/**
	 * Get the URI for use in location header for questions.  This is not nearly as important as we use HATEOAS, but do it to follow convention.
	 * @param isbn
	 * @param questionId
	 * @return
	 */
	protected URI getQuestionURIForUseInLocationHeader(Long isbn, Long questionId) {
		String uriStr = String.format(QUESTION_LOCATION_HEADER_FORMAT_STR, isbn, questionId);
		return URI.create(uriStr);
	}
}