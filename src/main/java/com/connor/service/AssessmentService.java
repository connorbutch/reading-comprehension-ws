package com.connor.service;

import java.util.List;

import com.connor.exception.AssessmentNotFoundException;
import com.connor.exception.DuplicateResourceException;
import com.connor.exception.InvalidRequestException;
import com.connor.model.Assessment;
import com.connor.model.Question;
import com.connor.model.dto.AssessmentDTO;
import com.connor.model.dto.QuestionDTO;

import org.apache.commons.lang3.tuple.ImmutablePair;

/**
 * Holds service api definitions (contains business logic)
 * @author connor
 *
 */
public interface AssessmentService {

	/**
	 * Gets an assessment for a given isbn
	 * @param isbn
	 * @throws InvalidRequestException when request is invalid
	 * @throws AssessmentNotFoundException if no assessment exists with this id
	 * @return
	 */
	Assessment getAssessmentForIsbn(Long isbn);	
	
	/**
	 * Adds an assessment for a given isbn
	 * @param isbnPathParameter
	 * @param assessment
	 * @throws InvalidRequestException when request is invalid
	 * @throws DuplicateResourceException if assessment already exists for isbn
	 */
	void addAssessment(Long isbn, AssessmentDTO assessment);
	
	/**
	 * 
	 * @param isbn
	 * @param isVerified
	 * @throws InvalidRequestException when request is invalid
 	 * @throws AssessmentNotFoundException if no assessment exists with this id
	 */
	void verifyAssessment(Long isbn, Boolean isVerified);
	
	/**
	 * 
	 * @param isbn
	 * @param question
	 * @return the id of the newly created question for use in uri
	 * @throws InvalidRequestException when request is invalid
	 * @throws AssessmentNotFoundException if no assessment exists with this id
	 */
	long addQuestion(Long isbn, QuestionDTO question);
	
	/**
	 * 
	 * @param questionId
	 * @return
	 * @throws InvalidRequestException when request is invalid
	 */
	Question getQuestionForId(Long questionId);
	
	/**
	 * Returns assessments matching the input criteria.
	 * 
	 * NOTE: This can return an empty list, but never null (choice was made not to use optional here 
	 * because controller wants to return list as empty if empty)
	 * @param limit
	 * @param offset
	 * @param createdTeacherId
	 * @return pair of assessments, and the limit used (in case they passed a limit greater than that allowed)
	 */
	ImmutablePair<List<Assessment>, Integer> getAssessments(Integer limit, Integer offset, Integer createdTeacherId);
	
	/**
	 * Used to get the total number of assessments.  If created teacher id is null, then 
	 * @param createdTeacherId
	 * @return
	 */
	long getTotalNumberOfMatchingAssessments(Integer createdTeacherId);
	
	
	
}
