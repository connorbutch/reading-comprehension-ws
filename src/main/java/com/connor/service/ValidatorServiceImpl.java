package com.connor.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.connor.exception.InvalidRequestException;
import com.connor.model.Assessment;
import com.connor.model.Question;
import com.connor.model.dto.AssessmentDTO;
import com.connor.model.dto.QuestionDTO;

/**
 * Used to validate requests.
 * @author connor
 *
 */
@Service
public class ValidatorServiceImpl implements ValidatorService {

	/**
	 * Default maximum reading level, can be overwritten by setting system property
	 */
	private static final double DEFAULT_MAXIMUM_READING_LEVEL = 12.0;

	/**
	 * Default minimum reading level, can be overwritten by setting system property
	 */
	private static final double DEFAULT_MINIMUM_READING_LEVEL = 0.1;

	/**
	 * 
	 */
	private static final double DEFAULT_MINIMUM_POINTS = 0.5;

	/**
	 * 
	 */
	private static final double DEFAULT_MAXIMUM_POINTS = 100;

	/**
	 * 
	 */
	private final Logger log;

	/**
	 * 
	 * @param log
	 */
	@Autowired
	public ValidatorServiceImpl(Logger log) {
		this.log = log;
	}

	@Override
	public void validateGetAssessmentByIsbn(Long isbn) {
		log.debug("Validating get assessment");

		List<String> errorMessages = new ArrayList<>();

		if(isbn == null) {
			errorMessages.add("Isbn cannot be null");
		}else if(isbn < 0) {
			errorMessages.add("Isbn cannot be negative");
		}

		if(!errorMessages.isEmpty()) {
			log.debug("Get assessment by isbn request was invalid");
			throw new InvalidRequestException(errorMessages); 
		}

		log.debug("The get request to retrieve a book by isbn was valid");		
	}

	@Override
	public void vaildateAddAssessment(Long isbn, AssessmentDTO assessment) {
		log.debug("Validating add assessment");

		List<String> errorMessages = new ArrayList<>();

		errorMessages.addAll(validateIsbn(isbn));

		if(assessment == null) {
			errorMessages.add("Assessment body cannot be null");
		}else {
			//check isbn path parameter should match that in the body
			if(isbn != null && isbn.longValue() != assessment.getIsbn()) {
				log.debug("Recieved different isbn values.  Isbn as path parameter is %d and one in body is %d", isbn, assessment.getIsbn());
				errorMessages.add("Path parameter isbn must match isbn passed in request body");
			}			
			if(assessment.getIsVerified()) {
				errorMessages.add("Test cannot be verified when it is first added");
			}			
			if(StringUtils.isBlank(assessment.getAuthorFirstName())) {
				errorMessages.add("Author first name is required"); //TODO maybe switch to using common format string
			}	
			if(StringUtils.isBlank(assessment.getAuthorLastName())) {
				errorMessages.add("Author last name is required");
			}
			if(StringUtils.isBlank(assessment.getTitle())) {
				errorMessages.add("Title is required"); //TODO maybe switch to using common format string
			}			
			errorMessages.addAll(validateReadingLevel(assessment.getReadingLevel()));			
			errorMessages.addAll(validateNumberOfPoints(assessment.getPoints()));	
			errorMessages.addAll(validateCreatedTeacherId(assessment.getCreatedTeacherId()));
		}

		if(!errorMessages.isEmpty()) {
			log.debug("Add assessment request was invalid");
			throw new InvalidRequestException(errorMessages); 
		}	

		log.debug("The post request to add an assessment {} for isbn {} is valid", assessment, isbn);
	}

	@Override
	public void validateVerifyAssessment(Long isbn, Boolean isVerified) {
		log.debug("Validating verify assessment");

		List<String> errorMessages = new ArrayList<>();
		
		errorMessages.addAll(validateIsbn(isbn));
		if(isVerified == null) {
			errorMessages.add("IsVerified is required");
		}
		
		if(!errorMessages.isEmpty()) {
			log.debug("Verify assessment request was invalid");
			throw new InvalidRequestException(errorMessages); 
		}	
	}

	@Override
	public void validateAddQuestion(Long isbn, QuestionDTO question) {
		log.debug("Validating add question request for isbn {} and question {}", isbn, question);
		
		List<String> errorMessages = new ArrayList<>();
		errorMessages.addAll(validateIsbn(isbn));
		
		if(question == null) {
			errorMessages.add("Question is required");
		}else {
			if(StringUtils.isAllBlank(question.getQuestionText())) {
				errorMessages.add("Question text is required");
			}
			if(StringUtils.isBlank(question.getCorrectAnswer())) {
				errorMessages.add("Correct answer is required");
			}
			if(CollectionUtils.isEmpty(question.getIncorrectAnswers())) {
				errorMessages.add("A minimum of one incorrect answer is required");
			}
		}
		
		if(!errorMessages.isEmpty()) {
			log.debug("Add question request was invalid");
			throw new InvalidRequestException(errorMessages);
		}
	}	
	
	@Override
	public void validateGetQuestion(Long questionId) {
		log.debug("Validating get question for question id {}", questionId);
		
		List<String> errorMessages = new ArrayList<>();
		
		errorMessages.addAll(validateQuestionId(questionId));
		
		if(!errorMessages.isEmpty()) {
			log.debug("Get question request was invalid");
			throw new InvalidRequestException(errorMessages);
		}
	}
	
	@Override
	public void validateGetAssessments(Integer limit, Integer offset, Integer createdTeacherId) {
		//NOTE: all three can be null (or any combination of them)
		log.debug("Validating get all assessments for limit {}, offset {}, and created teacher id {}", limit, offset, createdTeacherId);
		List<String> errorMessages = new ArrayList<>();
		
		if(limit != null && limit < 0) {
			errorMessages.add("Limit cannot be negative");
		}
		
		if(offset != null && offset < 0) {
			errorMessages.add("Offset cannot be negative");
		}
		
		errorMessages.addAll(validateCreatedTeacherId(createdTeacherId));
		
		if(!errorMessages.isEmpty()) {
			log.debug("Get all assessment request was invalid");
			throw new InvalidRequestException(errorMessages);
		}		
	}	
	
	/**
	 * 
	 * @param isbn
	 * @return
	 */
	protected List<String> validateIsbn(Long isbn){
		List<String> errorMessages = new ArrayList<>();		

		//NOTE: in future, could reach out to isbn book service here (written by say, national library association), but for now, just validate

		if(isbn == null) {
			errorMessages.add("Isbn is required");
		}else if(isbn < getMinIsbn()) {
			errorMessages.add(String.format("Isbn is invalid.  It must be greater than or equal to %f", getMinIsbn())); 
		}else if(isbn > getMaxIsbn()) {
			errorMessages.add(String.format("Isbn is invalid.  It must be less than or equal to %f", getMaxIsbn())); 
		}

		if(errorMessages.isEmpty()) {
			log.debug("Isbn {} is valid", isbn);
		}
		return errorMessages;		
	}

	/**
	 * 
	 * @param readingLevel
	 * @return
	 */
	protected List<String> validateReadingLevel(Double readingLevel){
		List<String> errorMessages = new ArrayList<>();		

		if(readingLevel == null) {
			errorMessages.add("Reading level is required");
		}else if(readingLevel > getMaxReadingLevel()) {
			errorMessages.add(String.format("Reading level is invalid.  It must be less than or equal to %f", getMaxReadingLevel())); 
		}else if(readingLevel < getMinReadingLevel()) {
			errorMessages.add(String.format("Reading level is invalid.  It must be greater than or equal to %f", getMinReadingLevel())); 
		}

		if(errorMessages.isEmpty()) {
			log.debug("The reading level {} is valid", readingLevel);
		}

		return errorMessages;
	}

	/**
	 * 
	 * @param numberOfPoints
	 * @return
	 */
	protected List<String> validateNumberOfPoints(Double numberOfPoints){
		List<String> errorMessages = new ArrayList<>();		

		if(numberOfPoints == null) {
			errorMessages.add("Points is required");
		}else {
			if(numberOfPoints <  getMinimumNumberOfPoints()) {
				errorMessages.add("Number of points is invalid"); //TODO add common format for too low
			}else if(numberOfPoints > getMaximumNuberOfPoints()) {
				errorMessages.add("Number of points is invalid");
			}
		}

		if(errorMessages.isEmpty()) {
			log.debug("The number of points {} is valid", numberOfPoints);
		}

		return errorMessages;
	}

	/**
	 * 
	 * @param createdTeacherId
	 * @return
	 */
	protected List<String> validateCreatedTeacherId(Integer createdTeacherId){
		List<String> errorMessages = new ArrayList<>();

		if(createdTeacherId == null) {
			errorMessages.add("Created teacher id cannot be null");
		}

		//NOTE intentional decision not to validate negative here to avoid duplicating logic between here and other service

		//NOTE: in future, would call out to teacher service here to validate; if we get a 404, reject

		if(errorMessages.isEmpty()) {
			log.debug("The created teacher id {} is valid", createdTeacherId);
		}

		return errorMessages;
	}
	
	/**
	 * 
	 * @param questionId
	 * @return
	 */
	protected List<String> validateQuestionId(Long questionId){
		List<String> errorMessages = new ArrayList<>();
		
		if(questionId == null) {
			errorMessages.add("Question id cannot be null");
		}else if(questionId < 0) {
			errorMessages.add("Question id cannot be negative");
		}
		
		if(errorMessages.isEmpty()) {
			log.debug("Question id {} is valid", questionId);
		}
		
		return errorMessages;
	}

	/**
	 * 
	 * @return
	 */
	protected long getMinIsbn() {
		return 0; //leave like this for now -- can overwrite calling function if wish to change behavior
	}

	/**
	 * 
	 * @return
	 */
	protected long getMaxIsbn() {
		return Long.MAX_VALUE; //leave like this for now -- can overwrite calling function if wish to change behavior
	}

	/**
	 * 
	 * @return
	 */
	protected double getMaxReadingLevel() {
		double maxReadingLevel = DEFAULT_MAXIMUM_READING_LEVEL;

		String defaultReadingLevel = System.getProperty("MAX_READING_LEVEL");
		if(defaultReadingLevel != null && canBeParsedToDouble(defaultReadingLevel)) {
			maxReadingLevel = Double.parseDouble(defaultReadingLevel);
			log.debug("Falling back on max reading level set in system property");
		}

		log.debug("Max reading level allowed is %f", maxReadingLevel);
		return maxReadingLevel;
	}

	/**
	 * 
	 * @return
	 */
	protected double getMinReadingLevel() {
		double minReadingLevel = DEFAULT_MINIMUM_READING_LEVEL;

		String minReadingLevelFromSysProperty = System.getProperty("MIN_READING_LEVEL");
		if(minReadingLevelFromSysProperty != null && canBeParsedToDouble(minReadingLevelFromSysProperty)) {
			minReadingLevel = Double.parseDouble(minReadingLevelFromSysProperty);
			log.debug("Falling back on min reading level set in system property");
		}

		log.debug("Min reading level allowed is %f", minReadingLevel);
		return minReadingLevel;
	}

	/**
	 * 
	 * @return
	 */
	protected double getMinimumNumberOfPoints() {
		return DEFAULT_MINIMUM_POINTS;
		//TODO allow to override with system property
	}

	/**
	 * 
	 * @return
	 */
	protected double getMaximumNuberOfPoints() {
		return DEFAULT_MAXIMUM_POINTS;
		//TODO allow to override with system property
	}

	/**
	 * 
	 * @param str
	 * @return
	 */
	private boolean canBeParsedToDouble(String str) {
		boolean isValid = true;

		try {
			Double.parseDouble(str);
		}catch(NumberFormatException e) {
			isValid = false;
		}catch(NullPointerException e) {
			isValid = false;
		}

		return isValid;
	}	
}