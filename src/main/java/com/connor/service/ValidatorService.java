package com.connor.service;

import com.connor.exception.InvalidRequestException;
import com.connor.model.Assessment;
import com.connor.model.Question;
import com.connor.model.dto.AssessmentDTO;
import com.connor.model.dto.QuestionDTO;

public interface ValidatorService {

	/**
	 * Validates get request, throwing exception if is invalid
	 * @throws InvalidRequestException if request is invalid
	 * @param isbn
	 */
	void validateGetAssessmentByIsbn(Long isbn);
	
	/**
	 * Validates an add request, throwing an exception if it is invalid.
	 * @param isbn
	 * @param assessment
	 */
	void vaildateAddAssessment(Long isbn, AssessmentDTO assessment);
	
	/**
	 * Validates a verify request, throwing exception if is invalid
	 * @param isbn
	 * @param isVerified
 	 * @throws InvalidRequestException if request is invalid
	 */
	void validateVerifyAssessment(Long isbn, Boolean isVerified);
	
	/**
	 * Validates an add question request, throwing exception if is invalid
	 * @param isbn
	 * @param question
	 * @throws InvalidRequestException if request is invalid
	 */
	void validateAddQuestion(Long isbn, QuestionDTO question);
	
	/**
	 * Validates get question, throwing exception if invalid
	 * @param questionId
	 * @throws InvalidRequestException if request is invalid
	 */
	void validateGetQuestion(Long questionId);
	
	/**
	 * Validates a request to get assessments, throwing exception if invalid
	 * @param limit
	 * @param offset
	 * @param createdTeacherId
	 * @throws InvalidRequestException if requets is invalid
	 */
	void validateGetAssessments(Integer limit, Integer offset, Integer createdTeacherId);
}