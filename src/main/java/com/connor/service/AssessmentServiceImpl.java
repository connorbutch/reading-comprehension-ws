package com.connor.service;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.connor.conversion.DTOToModelConverter;
import com.connor.dao.AssessmentDao;
import com.connor.exception.AssessmentNotFoundException;
import com.connor.exception.DuplicateResourceException;
import com.connor.exception.ReadingSystemException;
import com.connor.model.Assessment;
import com.connor.model.Question;
import com.connor.model.dto.AssessmentDTO;
import com.connor.model.dto.QuestionDTO;

/**
 * Implementation of assessment service
 * @author connor
 *
 */
@Service
@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRED, timeout = 10)
public class AssessmentServiceImpl implements AssessmentService{

	/**
	 * 
	 */
	private static final String NOT_FOUND_FORMAT_STR = "Assessment not found for isbn %d";
	
	/**
	 * Default limit value, used only if none is passed
	 */
	private static final int DEFAULT_LIMIT = 50;
	
	/**
	 * Default offset value, used only if none is passed
	 */
	private static final int DEFAULT_OFFSET = 0;
	
	/**
	 * While we can easily retrieve and return more than 500 assessments, we don't want to overload the client, so cap at 500.
	 * As with any functionality in this program, this can be overwritten if one chooses, but [lease be careful when doing so.
	 */
	private static final int MAX_LIMIT = 500;

	/**
	 * 
	 */
	private final Logger log;

	/**
	 * 
	 */
	private final ValidatorService validatorService;

	/**
	 * 
	 */
	private final DTOToModelConverter converter;

	/**
	 * 
	 */
	private final AssessmentDao assessmentDao;


	/**
	 * Constructor
	 * @param log
	 * @param validatorService
	 * @param converter
	 * @param assessmentDao
	 */
	@Autowired
	public AssessmentServiceImpl(Logger log, ValidatorService validatorService, DTOToModelConverter converter,
			AssessmentDao assessmentDao) {
		this.log = log;
		this.validatorService = validatorService;
		this.converter = converter;
		this.assessmentDao = assessmentDao;
	}

	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRED, timeout = 10, readOnly = true)
	@Override
	public Assessment getAssessmentForIsbn(Long isbn) {
		log.debug("In AssessmentServiceImpl.getAssessmentForIsbn for isbn {}", isbn);

		validatorService.validateGetAssessmentByIsbn(isbn);

		Assessment assessment = assessmentDao.getAssessmentForIsbn(isbn)
				.orElseThrow(() -> {
					return new AssessmentNotFoundException(getNotFoundMessage(isbn)); 
				});

		log.debug("Successfully found assesssment {} for isbn {}", assessment, isbn);
		return assessment;
	}	

	@Override
	public void addAssessment(Long isbnPathParameter, AssessmentDTO assessmentToAdd) {
		log.debug("In AssessmentServiceImpl.addAssessment for isbn {} and request body {}", isbnPathParameter, assessmentToAdd);

		validatorService.vaildateAddAssessment(isbnPathParameter, assessmentToAdd);

		Assessment assessment = converter.convertAssessmentDTO(isbnPathParameter, assessmentToAdd);
		/*
		 * I've seen some designs in which we check if resource already exists here, however, this is not as efficient; 
		 * as you can always run into a case where some other process/request adds the assessment between when you check and add it
		 * Hence, to me, it seems most logical to me, to try and run the insert, and if it fails, then handle the exceptions as they come
		 */

		boolean didUpdateSuccessfulWithoutDuplicateKey = assessmentDao.addAssessment(assessment);
		
		//this means we attempted to add assessment for one already existing, so throw exception for 409 (conflict) response
		if(!didUpdateSuccessfulWithoutDuplicateKey) {
			throw new DuplicateResourceException();
		}
		
		log.debug("At the end of AssessmentServiceImpl.addAssessment method without exception, this likely means assessment was added correctly");	
	}


	@Override
	public void verifyAssessment(Long isbn, Boolean isVerified) {
		log.debug("In AssessmentServiceImpl.verifyAssessment for isbn {} and verifictaion value {}", isbn, isVerified);

		validatorService.validateVerifyAssessment(isbn, isVerified);

		boolean didUpdate = assessmentDao.verifyAssessment(isbn, isVerified);

		if(!didUpdate) {
			throw new AssessmentNotFoundException(getNotFoundMessage(isbn));
		}

		log.debug("At the end of AssessmentServiceImpl.verifyAssessment.  This means we successfully verified assessment");
	}

	@Override
	public long addQuestion(Long isbn, QuestionDTO questionToAdd) {		
		log.debug("In AssessmentServiceImpl.addQuestion for isbn {} and question {}", isbn, questionToAdd);

		long questionId = -1;
		validatorService.validateAddQuestion(isbn, questionToAdd);

		Question question = converter.convertQuestionDTO(isbn, questionToAdd);

		//NOTE: in this case, we make a get first to check for assessment, because otherwise, we would need to put dao logic here, or business logic in dao.  This opens up to a slight risk if someone deletes an isbn between our check and our add question, we may return a 500 instead of a 404, but odds are sooooo low
		boolean doesAssessmentExist = assessmentDao.doesAssessmentExistWithIsbn(isbn);

		if(!doesAssessmentExist) {
			throw new AssessmentNotFoundException(getNotFoundMessage(isbn));
		}

		questionId = assessmentDao.addQuestion(question);		
		log.debug("At the end of AssessmentServiceImpl.addQuestion, we successfully added question with id {}", questionToAdd);
		return questionId;
	}

	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRED, timeout = 10, readOnly = true)
	@Override
	public Question getQuestionForId(Long questionId) {
		log.debug("In AssessmentServiceImpl.getQuestionForId for question id {}", questionId);

		validatorService.validateGetQuestion(questionId);

		Question question = assessmentDao.getQuestionForId(questionId)
				.orElseThrow(() -> {
					return new AssessmentNotFoundException(String.format("Question not found for id %d", questionId)); 
				});

		log.debug("At the end of AssessmentServiceImpl.getQuestionForId, with return value {}", question);
		return question;
	}
	
	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRED, timeout = 10, readOnly = true)
	@Override
	public ImmutablePair<List<Assessment>, Integer> getAssessments(Integer limit, Integer offset, Integer createdTeacherId) {
		log.debug("In AssessmentServiceImpl.getAssessments for limit {}, offset {}, and created teacher id {}", limit, offset, createdTeacherId);
		
		validatorService.validateGetAssessments(limit, offset, createdTeacherId);
		
		//NOTE: since it is allowed to have a null limit/offset value passed, if either (or both) are null, get the default then 
		if(limit == null) {
			limit = getDefaultLimit();
			log.debug("Limit was null, so using default value of {}", limit);
		}
		
		if(limit > getMaxLimit()) { //could be else if -- but just in case someone overrides and doesn't know what they are doing . . .
			log.debug("Limit value {} exceeds max limit value of {}.  We will set limit to max limit.", limit, getMaxLimit());
			limit = getMaxLimit(); //NOTE: If this happens, we return value to client
		}
		
		if(offset == null) {
			offset = getDefaultOffset();
			log.debug("Offset was null, so using default value of {}", offset);
		}
		
		List<Assessment> assessments; 

		if(createdTeacherId  == null) {
			log.debug("The created teacher id passed was null, so retrieving assessments for any teacher");
			assessments = assessmentDao.getAssessments(limit, offset);
		}else {
			log.debug("Only etrieving assessments created by teacher id {}", createdTeacherId);
			assessments = assessmentDao.getAssessmentsCreatedByTeacher(limit, offset, createdTeacherId);
		}				
				
		int numberOfAssessments = (assessments == null)? 0: assessments.size();
		
		log.debug("At the end of AssessmentServiceImpl.getAssessments returning list of size {}", numberOfAssessments);		
		return ImmutablePair.of(assessments, limit);
	}
	
	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRED, timeout = 10, readOnly = true)
	@Override
	public long getTotalNumberOfMatchingAssessments(Integer createdTeacherId) {
		log.debug("In AssessmentServiceImpl.getTotalNumberOfMatchingAssessments for created teacher id {}", createdTeacherId);
		
		long totalNumberOfMatchingAssessments;
		
		if(createdTeacherId == null) {
			log.debug("Retrieving total number of assessments");
			totalNumberOfMatchingAssessments = assessmentDao.getTotalNumberOfAssessments();
		}else {
			log.debug("Only retrieving total number of assessments created by teacher with id {}", createdTeacherId);
			totalNumberOfMatchingAssessments = assessmentDao.getTotalNumberOfAssessmentsCreatedByTeacher(createdTeacherId);
		}		
		
		log.debug("At the end of AssessmentServiceImpl.getTotalNumberOfMatchingAssessments for created teacher id {}.  Returning {}", createdTeacherId, totalNumberOfMatchingAssessments);
		return totalNumberOfMatchingAssessments;		
	}

	/**
	 * 
	 * @param isbn
	 * @return
	 */
	protected String getNotFoundMessage(long isbn) {
		return String.format(NOT_FOUND_FORMAT_STR, isbn);
	}	
	
	/**
	 * Get the default limit
	 * @return
	 */
	protected int getDefaultLimit() {
		return DEFAULT_LIMIT;
	}
	
	/**
	 * Get the default offset
	 * @return
	 */
	protected int getDefaultOffset() {
		return DEFAULT_OFFSET;
	}

	/**
	 * 
	 * @return
	 */
	protected int getMaxLimit() {
		return MAX_LIMIT;
	}	
}