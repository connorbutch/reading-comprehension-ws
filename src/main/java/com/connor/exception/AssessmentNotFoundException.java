package com.connor.exception;

/**
 * 
 * @author connor
 *
 */
public class AssessmentNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2481008919368080737L;

	/**
	 * 
	 * @param message
	 */
	public AssessmentNotFoundException(String message) {
		super(message);
	}
}
