package com.connor.exception;

import java.util.List;
import java.util.stream.Collector;

/**
 * Thrown when an invalid request is sent
 * @author connor
 *
 */
public class InvalidRequestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 82341112322L;
	
	/**
	 * 
	 */
	private final List<String> errorMessages;
	
	/**
	 * 
	 * @param errorMessages
	 */
	public InvalidRequestException(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	@Override
	public String getMessage() {		
		return errorMessages
				.stream()
				.collect(Collector.of(
						() -> new StringBuilder()
						,(stringBuilder, string) ->  stringBuilder.append(string)
						, (stringBuilder1, stringBuilder2) -> {
							stringBuilder1.append(stringBuilder2);
							return stringBuilder1;
						}
						, stringBuilder ->  stringBuilder.toString()
						, Collector.Characteristics.CONCURRENT,
		                Collector.Characteristics.UNORDERED
		                )
				);
	}
}