package com.connor.exception;

/**
 * This si the generic "catch all" linked to internal server errors.
 * @author connor
 *
 */
public class ReadingSystemException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4629975419278867231L;
	
	/**
	 * 
	 * @param message
	 */
	public ReadingSystemException(String message) {
		super(message);
	}
	
	/**
	 * 
	 * @param message
	 * @param t
	 */
	public ReadingSystemException(String message, Throwable t) {
		super(message, t);
	}
}