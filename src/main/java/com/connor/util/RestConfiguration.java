package com.connor.util;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;

/**
 * 
 * @author connor
 *
 */
@Configuration
public class RestConfiguration {

	/**
	 * Configures to automatically return json.
	 * @param configurer
	 */
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
	    configurer.favorPathExtension(true)
	    .favorParameter(false)
	    .ignoreAcceptHeader(true)
	    .useRegisteredExtensionsOnly(true)
	    .defaultContentType(MediaType.APPLICATION_JSON); 
	}	
}