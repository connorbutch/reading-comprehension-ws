package com.connor.util;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

/**
 * Allows a way of getting sql to run (allows us to externalize sql from code)
 * @author connor
 *
 */
@Configuration
@PropertySource("classpath:sql.properties")
public class SqlProvider {
	
	/**
	 * 
	 */
	private final Logger log;
	
	/**
	 * 
	 */
	private final Environment environment;

	/**
	 * 
	 * @param log
	 * @param environment
	 */
	@Autowired
	public SqlProvider(Logger log, Environment environment) {
		this.log = log;
		this.environment = environment;
	}
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public Optional<String> getSql(String key) {
		String sql = environment.getProperty(key);
		
		if(sql == null) {
			log.warn("When searching for property {}, the entry was not found in file", key);
		}else if(StringUtils.isBlank(sql)) {
			log.warn("When searching for property {}, the entry was empty", key);
		}else {
			log.debug("The sql for property {} is {}", key, sql);
		}
		
		return Optional.ofNullable(sql);
	}
}