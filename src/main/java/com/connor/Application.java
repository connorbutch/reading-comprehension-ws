package com.connor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Runner class.
 * @author connor
 *
 */
@SpringBootApplication
public class Application {

	/**
	 * Entry point for program.
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}