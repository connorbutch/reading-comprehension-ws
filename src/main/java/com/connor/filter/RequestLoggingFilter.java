package com.connor.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * Logs requests that do not go to a specified endpoint.
 * @author connor
 *
 */
@Component
public class RequestLoggingFilter extends OncePerRequestFilter {
	
	/**
	 * 
	 */
	private final Logger log;

	/**
	 * 
	 */
	private final RequestMappingHandlerMapping requestHandlerMapping;
	
	/**
	 * 
	 * @param log
	 * @param requestHandlerMapping
	 */
	@Autowired
	public RequestLoggingFilter(Logger log, RequestMappingHandlerMapping requestHandlerMapping) {
		this.log = log;
		this.requestHandlerMapping = requestHandlerMapping;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {		
		HandlerExecutionChain handlerExecutionChain = null;
		
		try {
			handlerExecutionChain = requestHandlerMapping.getHandler(request);
		}catch (Exception e) {
			//this indicates a serious, underlying exception, so wrap and rethrow
			throw new ServletException("An exception occurred when getting the handler for a method", e);
		}
		
		//handle request not mapped to an endpoint (will return 404)
		if(handlerExecutionChain == null) {
			handleRequestNotMappedToEndpoint(request);
		}
			
		//let the request continue being processed
		filterChain.doFilter(request, response);		
	}
	
	/**
	 * Called if we recieve a request that is not mapped to a given endpoint.
	 * @param request
	 */
	protected void handleRequestNotMappedToEndpoint(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder("Recieved a request that did not map to an endpoint");
		sb.append("The method type was: ");		
		sb.append(request.getMethod());
		sb.append(" The path of the request was: ");
		sb.append(request.getRequestURL());
		if(!StringUtils.isEmpty(request.getQueryString())) {
			sb.append(request.getQueryString());
		}
		sb.append(" The protocol was: ");
		sb.append(request.getProtocol());
		
		if(sb.length() > 0) {
			log.warn(sb.toString());
		}
	}
}