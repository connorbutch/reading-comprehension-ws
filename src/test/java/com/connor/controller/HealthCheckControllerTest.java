package com.connor.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.boot.actuate.health.Health;

/**
 * 
 * @author connor
 *
 */
@ExtendWith(MockitoExtension.class)
public class HealthCheckControllerTest {
	
	@Mock
	private Logger log;
	
	@InjectMocks
	private HealthCheckController controller;	
	
	@Test
	public void healthShouldReturnUp() {
		assertEquals(controller.health().getStatus(), Health.up().build().getStatus(), "Status should be up");
	}	
}