package com.connor.controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import com.connor.model.Assessment;
import com.connor.model.dto.AssessmentDTO;
import com.connor.service.AssessmentService;

@ExtendWith(MockitoExtension.class)
public class AssessmentControllerTest {

	@Mock
	private Logger log;
	
	@Mock
	private AssessmentService assessmentService;
	
	@Mock
	private RepresentationModelAssemblerSupport<Assessment, AssessmentDTO> assessmentAssembler;
	
	@InjectMocks
	private AssessmentController controller;
	
	@Test
	public void getAssessmentShouldReturnAssessment() {
		Mockito.when(assessmentService.getAssessmentForIsbn(Mockito.anyLong())).thenReturn(new Assessment());
		Mockito.when(assessmentAssembler.toModel(Mockito.any())).thenReturn(new AssessmentDTO());
		AssessmentDTO dto = controller.getAssessment(1L);
		assertNotNull(dto, "Response should not be null");
		assertTrue(true, "Get assessment should pass");
	}	
}