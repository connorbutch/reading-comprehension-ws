package com.connor.dao;


import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.connor.exception.ReadingSystemException;
import com.connor.model.Assessment;
import com.connor.util.SqlProvider;

@ExtendWith(MockitoExtension.class)
public class AssessmentDaoImplTest {

	@Mock
	private Logger log;
	
	@Mock
	private NamedParameterJdbcTemplate namedTemplate;
	
	@Mock
	private ResultSetExtractor<Assessment> assessmentExtractor;
	
	@Mock
	private SqlProvider sqlProvider;
	
	@InjectMocks
	private AssessmentDaoImpl dao;
	
	@Test
	public void getAssessmentForIsbnShouldThrowExceptionWhenPropertyNull() {
		Mockito.when(sqlProvider.getSql(Mockito.anyString()))
		.thenReturn(Optional.ofNullable(null));
		
		ReadingSystemException exception = assertThrows(ReadingSystemException.class, () -> dao.getAssessmentForIsbn(3));
		assertNotNull(exception.getMessage(), "Exception should have a message");
	}
	
	@Test
	public void getAssessmentForIsbnShouldThrowExceptionWhenDbException() {
		Mockito.when(sqlProvider.getSql(Mockito.anyString()))
		.thenReturn(Optional.ofNullable("SELECT 1"));
		Mockito.when(namedTemplate.query(Mockito.anyString(), Mockito.any(SqlParameterSource.class), Mockito.any(ResultSetExtractor.class)))
		.thenThrow(new DataAccessException("") { //abstract class, so much make annonmyous inner type
		});
		
		ReadingSystemException exception = assertThrows(ReadingSystemException.class, () -> dao.getAssessmentForIsbn(3));
		assertNotNull(exception.getMessage(), "Exception should have a message");
	}
	
	@Test
	public void getAssessmentForIsbnShouldReturnAssessment() {
		Mockito.when(sqlProvider.getSql(Mockito.anyString()))
		.thenReturn(Optional.ofNullable("SELECT 1"));
		Mockito.when(namedTemplate.query(Mockito.anyString(), Mockito.any(SqlParameterSource.class), Mockito.any(ResultSetExtractor.class)))
		.thenReturn(new Assessment());
		
		Optional<Assessment> assessment = dao.getAssessmentForIsbn(3);
		assertTrue(assessment.isPresent(), "Assessment should be returned");
	}
	
	@Test
	public void getAssessmentForIsbnShouldReturnNullAssessmentWhenNotFound() {
		Mockito.when(sqlProvider.getSql(Mockito.anyString()))
		.thenReturn(Optional.ofNullable("SELECT 1"));
		Mockito.when(namedTemplate.query(Mockito.anyString(), Mockito.any(SqlParameterSource.class), Mockito.any(ResultSetExtractor.class)))
		.thenReturn(null);
		
		Optional<Assessment> assessment = dao.getAssessmentForIsbn(3);
		assertTrue(assessment.isEmpty(), "Assessment should not be returned");
	}
	
	@Test
	public void addAssessmentShouldThrowExceptionWhenPropertyNull() {
		Mockito.when(sqlProvider.getSql(Mockito.anyString()))
		.thenReturn(Optional.ofNullable(null));
		
		ReadingSystemException exception = assertThrows(ReadingSystemException.class, () -> dao.addAssessment(null));
		assertNotNull(exception.getMessage(), "Exception should have a message");
	}
	
	@Test
	public void addAssessmentShouldSucceed() {
		//TODO look into this
		//this leads to UnnecessaryStubbing exception 
//		Mockito.when(namedTemplate.update(Mockito.anyString(), Mockito.any(SqlParameterSource.class))).thenReturn(4);
//		Mockito.when(sqlProvider.getSql(Mockito.anyString()))
//		.thenReturn(Optional.ofNullable("SELECT 1"));
	}
	
	@Test
	public void addAssessmentShouldThrowExceptionWhenDbException() {
		Mockito.when(namedTemplate.update(Mockito.anyString(), Mockito.any(SqlParameterSource.class))).thenThrow(new DataAccessException("") { //abstract class, so much make annonmyous inner type
		});
		Mockito.when(sqlProvider.getSql(Mockito.anyString()))
		.thenReturn(Optional.ofNullable("SELECT 1"));
		
		ReadingSystemException exception = assertThrows(ReadingSystemException.class, () -> dao.addAssessment(new Assessment()));
		assertNotNull(exception.getMessage(), "Exception should have a message");	
	}
	
	@Test
	public void verifyAssessmentShouldThrowExceptionWhenPropertyNull() {
		Mockito.when(sqlProvider.getSql(Mockito.anyString())).thenReturn(Optional.ofNullable(null));
		ReadingSystemException exception = assertThrows(ReadingSystemException.class, () -> dao.verifyAssessment(123l, false));
		assertNotNull(exception, "Exception should not be null");
	}
	
	@Test
	public void verifyAssessmentShouldThrowExceptionWhenDbException() {
		Mockito.when(namedTemplate.update(Mockito.anyString(), Mockito.any(SqlParameterSource.class))).thenThrow(new DataAccessException("") { //abstract class, so much make annonmyous inner type
		});
		Mockito.when(sqlProvider.getSql(Mockito.anyString())).thenReturn(Optional.ofNullable("SELECT 1"));
		ReadingSystemException exception = assertThrows(ReadingSystemException.class, () -> dao.verifyAssessment(123l, false));	
	}
	
	@Test
	public void verifyAssessmentShouldReturnTrueIfUpdates() {
		Mockito.when(sqlProvider.getSql(Mockito.anyString())).thenReturn(Optional.of("SELECT 1"));
		Mockito.when(namedTemplate.update(Mockito.anyString(), Mockito.any(SqlParameterSource.class))).thenReturn(0);
		assertTrue(!dao.verifyAssessment(123l, false));
	}
	
	@Test
	public void verifyAssessmentShouldReturnFalseIfNoUpdates() {
		Mockito.when(sqlProvider.getSql(Mockito.anyString())).thenReturn(Optional.of("SELECT 1"));
		Mockito.when(namedTemplate.update(Mockito.anyString(), Mockito.any(SqlParameterSource.class))).thenReturn(2);
		assertTrue(dao.verifyAssessment(123l, false));
	}
	
	@Test
	public void addQuestionShouldThrowExceptionWhenPropertyNull() {
		
	}
	
	@Test
	public void addQuestionShouldThrowExceptionWhenDbException() {
		
	}
	
	@Test
	public void addQuestionShouldSucceed() {
		
	}
}