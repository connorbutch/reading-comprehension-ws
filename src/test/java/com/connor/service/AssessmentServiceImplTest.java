package com.connor.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.dao.DuplicateKeyException;

import com.connor.exception.AssessmentNotFoundException;
import com.connor.exception.DuplicateResourceException;
import com.connor.exception.ReadingSystemException;
import com.connor.model.Assessment;
import com.connor.model.dto.AssessmentDTO;
import com.connor.conversion.DTOToModelConverter;
import com.connor.dao.AssessmentDao;

@ExtendWith(MockitoExtension.class)
public class AssessmentServiceImplTest {

	@Mock
	private Logger log;

	@Mock
	private ValidatorService validatorService;

	@Mock
	private AssessmentDao assessmentDao;
	
	@Mock
	private DTOToModelConverter converter;
	
	@InjectMocks
	private AssessmentServiceImpl assessmentService;
	
	@Test
	public void getAssessmentForIsbnShouldThrow404WhenNotFound() {
		Mockito.when(assessmentDao.getAssessmentForIsbn(Mockito.anyLong()))
		.thenReturn(Optional.ofNullable(null));
		
		AssessmentNotFoundException notFoundException = assertThrows(AssessmentNotFoundException.class, () -> assessmentService.getAssessmentForIsbn(3L));
		assertNotNull(notFoundException.getMessage(), "Exception should have message");
	}
	
	@Test
	public void getAssessmentForIsbnShouldSucceed() {
		Mockito.when(assessmentDao.getAssessmentForIsbn(Mockito.anyLong()))
		.thenReturn(Optional.of(new Assessment()));
		
		Assessment assessment = assessmentService.getAssessmentForIsbn(3L);
		assertNotNull(assessment, "Assessment should be returned");
	}	
	
	@Test
	public void addAssessmentShouldThrowDuplicateKeyAsConflict() {
		Mockito.when(assessmentDao.addAssessment(Mockito.any())).thenReturn(false);
		
		DuplicateResourceException duplicateResourceException = assertThrows(DuplicateResourceException.class, () -> assessmentService.addAssessment(4L, new AssessmentDTO()));
		//assertNotNull(duplicateResourceException.getMessage(), "Exception should have message");
	}
	
	@Test
	public void addAssessmentShouldRethrowNonduplicateKey() {
		Mockito.doThrow(new ReadingSystemException("dummy")).when(assessmentDao).addAssessment(Mockito.any());

		ReadingSystemException duplicateResourceException = assertThrows(ReadingSystemException.class, () -> assessmentService.addAssessment(4L, new AssessmentDTO()));
	}
	
	@Test
	public void addAssessmentShouldSucceed() {
		Mockito.when(converter.convertAssessmentDTO(Mockito.anyLong(), Mockito.any())).thenReturn(new Assessment());
		Mockito.when(assessmentDao.addAssessment(Mockito.any())).thenReturn(true);
		assessmentService.addAssessment(3L, new AssessmentDTO());
		assertTrue(true, "Should succeed");
	}
	
	
}