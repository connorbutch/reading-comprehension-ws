package com.connor.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.connor.exception.InvalidRequestException;

@ExtendWith(MockitoExtension.class)
public class ValidatorServiceImplTest {

	@Mock
	private Logger log;

	@InjectMocks
	private ValidatorServiceImpl validatorService;

	@Test
	public void validateGetAssessmentByIsbnShouldFailOnNullIsbn() {
		InvalidRequestException invalidRequestException = assertThrows(InvalidRequestException.class, () -> validatorService.validateGetAssessmentByIsbn(null));
		assertNotNull(invalidRequestException.getMessage(), "Exception should have message");
	}

	@Test
	public void validateGetAssessmentByIsbnShouldFailOnNegativeIsbn() {
		InvalidRequestException invalidRequestException = assertThrows(InvalidRequestException.class, () -> validatorService.validateGetAssessmentByIsbn(-3L));
		assertNotNull(invalidRequestException.getMessage(), "Exception should have message");
	}
	
	@Test
	public void validateGetAssessmentByIsbnShouldSucceed() {
		validatorService.validateGetAssessmentByIsbn(14L);
		assertTrue(true, "Validation should succeeed");
	}
	
	
}