package com.connor.advice;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.connor.exception.AssessmentNotFoundException;
import com.connor.exception.DuplicateResourceException;
import com.connor.exception.InvalidRequestException;
import com.connor.exception.ReadingSystemException;
import com.connor.model.dto.ApiErrorDTO;

@ExtendWith(MockitoExtension.class)
public class RestExceptionHandlerTest {

	@Mock
	private Logger log;
	
	@InjectMocks
	private RestExceptionHandler handler;
	
	@Test
	public void handleInvalidRequestShouldReturnError() {
		ResponseEntity<ApiErrorDTO> response = handler.handleInvalidRequest(new InvalidRequestException(Arrays.asList("Please pass an isbn")));
		assertEquals(response.getStatusCodeValue(), HttpStatus.BAD_REQUEST.value(), "Status code should be that of bad request");
		assertTrue(response.getBody() instanceof ApiErrorDTO, "Response body should be an api error");	
	}	
	
	@Test
	public void handleNotFoundShouldReturnError() {
		ResponseEntity<ApiErrorDTO> response = handler.handleNotFound(new AssessmentNotFoundException(null));
		assertEquals(response.getStatusCodeValue(), HttpStatus.NOT_FOUND.value(), "Status code should be that of not found");
		assertTrue(response.getBody() instanceof ApiErrorDTO, "Response body should be an api error");	
	}
	
	@Test
	public void handleSystemExceptionShouldReturnError() {
		ResponseEntity<ApiErrorDTO> response = handler.handleSystemException(new ReadingSystemException(""));
		assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR, "Status code should be internal server error");
		assertTrue(response.getBody() instanceof ApiErrorDTO, "Response body should be an api error");	
	}
	
	@Test
	public void handleDuplicateResourceExceptionShouldReturnError() {
		ResponseEntity<ApiErrorDTO> response = handler.handleDuplicateResourceException(new DuplicateResourceException());
		assertEquals(response.getStatusCode(), HttpStatus.CONFLICT, "Http status code should match");
		assertTrue(response.getBody() instanceof ApiErrorDTO, "Response body should be an api error");
	}
	
	/**
	 * This method is used in an attempt to catch issues at build (test) time, instead of weird runtime issues.
	 * This checks type in annotation matches type in method, which, in my opinion, should be a compile time check
	 */
	@Test
	public void exceptionInAnnotationShouldMatchParameter() {
//		Arrays.asList(RestExceptionHandler.class.getMethods())
//		.stream()
//		.filter((method) -> { 
//			return method.isAnnotationPresent(ExceptionHandler.class);
//		}).forEach((method) -> {
//			assertNotNull(method.getAnnotation(ExceptionHandler.class).value(), "Each annotation should contain the argument");
//			assertEquals(1, method.getParameterCount(), "Each exception handler should have exactly one argument");
//			//TODO fix this, it currently fails saying: <com.connor.exception.InvalidRequestException> but was: <[class com.connor.exception.InvalidRequestException]>
//			//assertEquals(method.getParameters()[0].getType(), method.getAnnotation(ExceptionHandler.class).value(), "Type in annotation should match type of argument");
//		});
		
	}
}