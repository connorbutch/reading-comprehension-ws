package com.connor.model;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class ErrorEnumTest {

	@Test
	public void errorEnumHasToHaveUniqueId() {
		Set<Integer> idValues = new HashSet<>();
		for(ErrorEnum errorEnum: ErrorEnum.values()) {
			assertTrue(idValues.add(errorEnum.getErrorCode()), "Each error must have it's own id");
		}
	}	
}