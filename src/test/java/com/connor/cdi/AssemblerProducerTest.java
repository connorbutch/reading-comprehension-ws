package com.connor.cdi;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import com.connor.model.Assessment;
import com.connor.model.dto.AssessmentDTO;

@ExtendWith(MockitoExtension.class)
public class AssemblerProducerTest {

	@InjectMocks
	private AssemblerProducer producer;
	
	@Test
	public void getAssessmentAssemblerShouldReturn() {
		RepresentationModelAssemblerSupport<Assessment, AssessmentDTO> assembler = producer.getAssessmentAssembler(null);
		assertNotNull(assembler, "Producer method should yield bean result");
	}	
}