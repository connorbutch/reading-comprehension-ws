package com.connor.cdi;

import static org.junit.jupiter.api.Assertions.*;


import java.lang.reflect.Field;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.beans.factory.InjectionPoint;

/**
 * 
 * @author connor
 *
 */
@ExtendWith(MockitoExtension.class)
public class LoggerProducerTest {

	@InjectMocks
	private LoggerProducer loggerProducer;

	@Test
	public void getLoggerShouldReturnLogger() {
		Field field = null;
		try {
			field = LoggerProducerTest.class.getDeclaredField("loggerProducer");			
		} catch (NoSuchFieldException | SecurityException e) {
			throw new RuntimeException(e);
		}

		Logger log = loggerProducer.getLogger(new InjectionPoint(field));
		assertNotNull(log, "Logger producer should create log");
	}	
}