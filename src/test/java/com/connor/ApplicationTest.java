//package com.connor;
//
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.powermock.api.mockito.PowerMockito;
//import org.powermock.core.classloader.annotations.PowerMockIgnore;
//import org.powermock.core.classloader.annotations.PrepareForTest;
//import org.powermock.modules.junit4.PowerMockRunner;
//import org.springframework.boot.SpringApplication;
//
//
////NOTE: For some reason, powermock is being a pain.  I'm not really worried about this, as this isn't testing any logic we have written
//@RunWith(PowerMockRunner.class) //run with powermock for our static methods
//@PrepareForTest({SpringApplication.class})
//@PowerMockIgnore({"com.sun.org.apache.xerces.*", "javax.xml.*", "org.xml.*", "javax.management.*"}) //java.lang.IllegalAccessError: class javax.xml.parsers.FactoryFinder (in unnamed module @0x1e141e42) cannot access class jdk.xml.internal.SecuritySupport (in module java.xml) because module java.xml does not export jdk.xml.internal to unnamed module @0x1e141e42
//public class ApplicationTest {
//	@Test
//	public void mainShouldRun() {
//		PowerMockito.mockStatic(SpringApplication.class);
//		Application.main(null);
//	}
//}