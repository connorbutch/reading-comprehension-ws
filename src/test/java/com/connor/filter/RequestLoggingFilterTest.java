package com.connor.filter;

import static org.junit.jupiter.api.Assertions.assertThrows;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import io.restassured.response.ValidatableResponse;

@ExtendWith(MockitoExtension.class)
public class RequestLoggingFilterTest {

	@Mock
	private Logger log;
	
	@Mock
	private RequestMappingHandlerMapping requestHandlerMapping;
	
	@InjectMocks
	private RequestLoggingFilter requestLoggingFilter;
	
	@Test
	public void doFilterInternalShouldRethrowException() throws Exception {
		//TODO may need to decide if need to test protected here with powermock . . . .
		
		ValidatableResponse response = null;;
		
		

		
		//		Mockito.when(requestHandlerMapping.getHandler(Mockito.any()))
//		.thenThrow(new Exception());
//		
//		HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
//		HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
//		FilterChain filterChain = Mockito.mock(FilterChain.class);
//		assertThrows(ServletException.class, requestLoggingFilter.doFilterInternal(request, response, filterChain));	
	}
}
