package com.connor.assembler;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.connor.controller.AssessmentController;
import com.connor.conversion.AssessmentAssemblerImpl;
import com.connor.model.Assessment;
import com.connor.model.dto.AssessmentDTO;

@ExtendWith(MockitoExtension.class)
public class AssessmentAssemblerImplTest {

	/**
	 * 
	 */
	private static AssessmentAssemblerImpl assembler;
	
	@BeforeAll
	public static void init() {
		Logger log = Mockito.mock(Logger.class);
		assembler = new AssessmentAssemblerImpl(AssessmentController.class, AssessmentDTO.class, log);
	}

	@Test
	public void toModelShouldCopyFields() {
		//TODO
		Assessment assessment = new Assessment();
		AssessmentDTO dto = assembler.toModel(assessment);
		assertNotNull(dto, "assembler should construct  object");
	}	
}
