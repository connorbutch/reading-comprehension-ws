package com.connor.util;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.core.env.Environment;

@ExtendWith(MockitoExtension.class)
public class SqlProviderTest {

	@Mock
	private Logger log;

	@Mock
	private Environment environment;

	@InjectMocks
	private SqlProvider sqlProvider;

	@Test
	public void getSqlShouldLogNull() {
		Mockito.when(environment.getProperty(Mockito.anyString())).thenReturn(null);
		Optional<String> sql = sqlProvider.getSql("test");
		assertNotNull(sql, "Optional should not be null");
		assertTrue(!sql.isPresent(), "Value should not be present");
	}

	@Test
	public void getSqlShouldLogBlank() {
		String toReturn = "    ";
		Mockito.when(environment.getProperty(Mockito.anyString())).thenReturn(toReturn);
		Optional<String> sql = sqlProvider.getSql("test");
		assertNotNull(sql, "Optional should not be null");
		assertTrue(sql.isPresent(), "Value should be present");
		assertEquals(sql.get(), toReturn, "Value should be what comes from properties file");
	}

	@Test
	public void getSqlShouldLogAll() {
		String toReturn = "SELECT 1";
		Mockito.when(environment.getProperty(Mockito.anyString())).thenReturn(toReturn);
		Optional<String> sql = sqlProvider.getSql("test");
		assertNotNull(sql, "Optional should not be null");
		assertTrue(sql.isPresent(), "Value should be present");
		assertEquals(sql.get(), toReturn, "Value should be what comes from properties file");
	}
}