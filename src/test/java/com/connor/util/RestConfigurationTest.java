package com.connor.util;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;

@ExtendWith(MockitoExtension.class)
public class RestConfigurationTest {

	@InjectMocks
	private RestConfiguration restConfiguration;
	
	@Test
	public void testConfigurationShouldPass() {
		ContentNegotiationConfigurer configurer = new ContentNegotiationConfigurer(null);
		restConfiguration.configureContentNegotiation(configurer);
		assertTrue(true, "Should pass test configuration");
	}	
}