#Author: Connor Butch
Feature: Health check

  Scenario: Health check should return healthy once server is started
    Given The server is started
    When a health check request is made   
    Then the response will indicate the server is healthy