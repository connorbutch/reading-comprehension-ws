# reading-comprehension-ws

This project contains the business logic and database interactions for the reading comprehension program.  Features of this include:
spring boot
docker
kubernetes
cucumber 
health check
gitlab
aws